<!DOCTYPE html>
<html>

<head>
    <!-- <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;500;600&display=swap" rel="stylesheet"> -->
    <style>
        p {
            font-size: 12px;
            font-family: 'Courier New', Courier, monospace;
        }

        .main {
            width: 100%;
            height: 100px;
        }

        .left{
            width: 100%;
            margin-bottom: 20px;
        }
    </style>
</head>

<body>
    <div class="left">
        <strong>{{$data->company}}</strong>
        <p style="font-size: 18px;">PAYSLIP FOR {{$data->month}} / {{$data->year}}</p>
        <div style="border-bottom:1px black solid"></div>
        <p>NAME : {{$data->empname}}</p>
        <p>PAY PERIOD : END-{{$data->month}}-{{$data->year}}</p>
        <p>I/C # : {{$data->empnirc}}</p>
        <p>CREATED AT # : {{$data->created_at}}</p>
        <p>PRINT ON # : {{$data->print_on}}</p>
        <div style="border-bottom:1px black solid"></div>
    </div>
    <table style="width: 100%;">
        <tbody>
        <tr>
        <td style="width:35% ;border-right:1px black solid">
            <p style="text-align: center;">--  EARNINGS --</p>
            <p style="padding-left: 10px;">BASIC PAY</p>
            <p style="padding-left: 10px;">ADDITIONAL PAY</p>
            <p style="padding-left: 10px;">OT</p>
            <p style="padding-left: 10px;">BONUS</p>
            <p style="padding-left: 10px;">MEAL</p>
            <p style="padding-left: 10px;">CLAIMS</p>
            <p style="padding-left: 10px;">INCENTIVES 1</p>
            <p style="padding-left: 10px;">INCENTIVES 2</p>
            <p style="padding-left: 10px;">ATTN ALLOWANCE</p>
            <p style="padding-left: 10px;">BACK PAY</p>
            <p style="padding-left: 10px;">PLENCASH</p>
            <p style="padding-left: 10px;">PROJALLOW</p>
            <p style="padding-left: 10px;">CNYSPCOM</p>
            <p style="padding-left: 10px;">REFERRAL</p>
            <p style="text-align: right; padding-right:10px; font-weight:bold">TOTAL</p>
            <p style="text-align: right; padding-right:10px; font-weight:bold">GROSS PAY</p>
        </td>
        <td style="width:15% ;border-right:1px black solid">
            <p style="text-align: center;">--  RM --</p>
            <p style="text-align: right; padding-right:10px">{{$data->basic}}</p>
            <p style="text-align: right; padding-right:10px">{{$data->addpay}}</p>
            <p style="text-align: right; padding-right:10px">{{$data->ot}}</p>
            <p style="text-align: right; padding-right:10px">{{$data->bonus}}</p>
            <p style="text-align: right; padding-right:10px">{{$data->meal}}</p>
            <p style="text-align: right; padding-right:10px">{{$data->claims}}</p>
            <p style="text-align: right; padding-right:10px">{{$data->inc1}}</p>
            <p style="text-align: right; padding-right:10px">{{$data->other2}}</p>
            <p style="text-align: right; padding-right:10px">{{$data->other3}}</p>
            <p style="text-align: right; padding-right:10px">{{$data->other4}}</p>
            <p style="text-align: right; padding-right:10px">{{$data->plencash}}</p>
            <p style="text-align: right; padding-right:10px">{{$data->projallow}}</p>
            <p style="text-align: right; padding-right:10px">{{$data->cnyspcom}}</p>
            <p style="text-align: right; padding-right:10px">{{$data->referral}}</p>
            <p style="text-align: right; padding-right:10px">{{$data->grosspay}}</p>
            <p style="text-align: right; padding-right:10px">{{$data->grosspay}}</p>
        </td>
        <td style="width:35%; border-right:1px black solid">
            <p style="text-align: center;">--  DEDUCTION --</p>
            <p style="padding-left: 10px;">EMPLOYEE EPF (KWSP)</p>
            <p style="padding-left: 10px;">EMPLOYEE SOCSO (PERKESO)</p>
            <p style="padding-left: 10px;">EMPLOYEE EIS</p>
            <p style="padding-left: 10px;">PCB</p>
            <p style="padding-left: 10px;">NON PAY LEAVE</p>
            <p style="padding-left: 10px;">HOSTEL</p>
            <p style="padding-left: 10px;">DISPUTE</p>
            <p style="text-align: center;">&nbsp;</p>
            <p style="text-align: center;">&nbsp;</p>
            <p style="text-align: center;">&nbsp;</p>
            <p style="text-align: center;">&nbsp;</p>
            <p style="text-align: center;">&nbsp;</p>
            <p style="text-align: center;">&nbsp;</p>
            <p style="text-align: center;">&nbsp;</p>
            <p style="text-align: right; padding-right:10px; font-weight:bold">TOTAL DEDUCTION</p>
            <p style="text-align: right; padding-right:10px; font-weight:bold">NETT PAY</p>
        </td>
        <td style="width:15%">
            <p style="text-align: center;">--  RM  --</p>
            <p style="text-align: right; padding-right:10px">{{$data->empepf}}</p>
            <p style="text-align: right; padding-right:10px">{{$data->empsocso}}</p>
            <p style="text-align: right; padding-right:10px">{{$data->empeis}}</p>
            <p style="text-align: right; padding-right:10px">{{$data->emppcb}}</p>
            <p style="text-align: right; padding-right:10px">{{$data->npl}}</p>
            <p style="text-align: right; padding-right:10px">{{$data->hostel}}</p>
            <p style="text-align: right; padding-right:10px">{{$data->dispute}}</p>
            <p style="text-align: right; padding-right:10px">&nbsp;</p>
            <p style="text-align: right; padding-right:10px">&nbsp;</p>
            <p style="text-align: right; padding-right:10px">&nbsp;</p>
            <p style="text-align: right; padding-right:10px">&nbsp;</p>
            <p style="text-align: right; padding-right:10px">&nbsp;</p>
            <p style="text-align: right; padding-right:10px">&nbsp;</p>
            <p style="text-align: right; padding-right:10px">&nbsp;</p>
            <p style="text-align: right; padding-right:10px">{{$data->grosspay - $data->nettpay}}</p>
            <p style="text-align: right; padding-right:10px">{{$data->nettpay}}</p>
        </td>
        </tr>
        </tbody>
        </table>
        <div style="border-bottom:1px black solid"></div>
        <p>CURRENT-MONTH</p>
        <table>
        <tbody>
        <tr>
        <td>&nbsp;</td>
        <td><p style="padding-right:10px; font-weight:bold">E.P.F.</p></td>
        <td><p style="padding-right:10px; font-weight:bold">SOCSO</p></td>
        <td><p style="padding-right:10px; font-weight:bold">EIS</p></td>
        <td><p style="padding-right:10px; font-weight:bold">TAX</p></td>
        </tr>
        <tr>
        <td><p style="padding-right:10px; font-weight:bold">EMP'EE:</p></td>
        <td><p style="padding-right:10px; text-align:right">RM {{$data->empepf}}</p></td>
        <td><p style="padding-right:10px; text-align:right">RM {{$data->empsocso}}</p></td>
        <td><p style="padding-right:10px; text-align:right">RM {{$data->empeis}}</p></td>
        <td><p style="padding-right:10px; text-align:right">RM {{$data->emppcb}}</p></td>
        </tr>
        <tr>
        <td><p style="padding-right:10px; font-weight:bold">EMP'ER:</p></td>
        <td><p style="padding-right:10px; text-align:right">RM {{$data->repf}}</p></td>
        <td><p style="padding-right:10px; text-align:right">RM {{$data->rsocso}}</p></td>
        <td><p style="padding-right:10px; text-align:right">RM {{$data->reis}}</p></td>
        <td><p style="padding-right:10px">&nbsp;</p></td>
        </tr>
        <tr>
        <td><p style="padding-right:10px; font-weight:bold">TOTAL:</p></td>
        <td><p style="padding-right:10px; text-align:right">RM {{$data->empepf + $data->repf}}</p></td>
        <td><p style="padding-right:10px; text-align:right">RM {{$data->empsocso + $data->rsocso}}</p></td>
        <td><p style="padding-right:10px; text-align:right">RM {{$data->empeis + $data->reis}}</p></td>
        <td><p style="padding-right:10px; text-align:right">RM {{$data->emppcb}}</p></td>
        </tr>
        </tbody>
        </table>
        <p>This is a computer-generated document. No signature is required.</p>
</body>

</html>