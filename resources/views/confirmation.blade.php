<!DOCTYPE html>
<html>

<head>
    <!-- <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;500;600&display=swap" rel="stylesheet"> -->
    <style>
        p {
            font-size: 12px;
        }
    </style>
</head>

<body>
    <h1 style="text-align: center; color:#6a99d0; border-bottom: #6a99d0 1px solid; padding-top:10px; padding-bottom:10px">
        Letter of Appointment</h1>
    <h1 style="text-align: center;">{{$data->fullname}}</h1>
    <p style="text-align: center; font-weight: bold; font-size: larger;"">ASIA FIBRE SOLUTIONS SDN BHD (1286068-M)</p>
    <h5 style=" text-align: center;">{{$data->effdate}}</h5>
        <br>
        <br>
        <br>
        <br>
        <br>
    <p>{{$data->fullname}}</p>
    <p>{{$data->address1}},</p>
    <p>{{$data->address2}},</p>
    <p>{{$data->address3}},</p>
    <p>{{$data->postcode}},</p>
    <p>{{$data->state}}, {{$data->country}}</p>
    <br>
    <br>
    <h3>Letter of Appointment</h3>
    <p>We are pleased to confirm your appointment as Telemarketing Executive with <strong>Asia Fibre Solutions Sdn.
            Bhd.</strong> (the“Company”) effective <strong>{{$data->effdate}}</strong>.</p>
    <p>We expect you to treat this Employment Contract (hereafter referred to as this or the “Contract”) with upmost privacy
        and confidentiality. It shall not be valid without the signature and company stamp of the company’s President. No
        portion or provision of this contract may be reproduced, transmitted in any form or by any means, electronic or
        mechanical, including photocopying and recording or by any information or retrieval system without the written
        consent and approval of the Company. Any violation hereof, shall be dealt with accordingly, including but not
        limited to the withdrawal of the offer or annulment of the contract.</p>
    <h3>Terms and Conditions</h3>
    <p>This offer is contingent on the following:</p>
    <p style="margin-left:2%;">1. Your representation that you are not subject to any other employment agreement that
        would prevent you from
        performing your duties in the Company as of the aforementioned effective date.</p>
    <p style="margin-left:2%;">2. Favorable results of background investigation and completion of pre-employment
        requirements:</p>
    <p style="margin-left:5%;">a. You warrant that you have a good credit standing and are not otherwise heavily
        indebted beyond your means. You
        have not committed, nor have been accused of having committed a crime. You’re afflicted with any incurable disease
        or physical condition that could adversely affect work performance; and that you’ve not been dismissed for cause,
        forced to resign or denied employment previously.</p>
    <p style="margin-left:5%;">b. You understand that any misrepresentation in this regard may give rise to your
        dismissal for cause from the
        Company.</p>
    <p style="margin-left:5%;">c. Your submission of the required documents.</p>
    <br>
    <h3>Compensation Package</h3>
    <p style="margin-left:2%;">1. Your employment will commence on {{$data->effdate}} with one year renewable contract.</p>
    <p style="margin-left:2%;">2. You will receive an equivalent monthly basic salary of RM payable no later than 7th of each calendar month. This
        amount remains the same regardless of the number of days in a calendar month, subject to deductions for unauthorized or
        unexcused absences.</p>
    <p style="margin-left:2%;">3. Your salary will be subject to the necessary deductions in respect of income tax and statutory contributions in
        accordance with the prevailing statutory rates.</p>
    <p style="margin-left:2%;">4. Commission Scheme
        Commission will be processed with payroll after end of each month. This commission scheme might change time to time to
        fit the operations. It will be shared by your reporting manager accordingly.</p>
    <p style="margin-left:2%;">5. Please see attachment for a full description of your benefits.</p>
    <h3>Probationary Period</h3>
    <p>You will in the first instance be required to serve a minimum probationary of Three (3) months. On completion of the
        said period subject to the satisfaction of the Company, you will be confirmed in your appointment, and you will receive
        written notification of your status as a full employee.</p>

    <p>You understand that the Company can terminate your employment during this probationary period for any just or authorized
        cause, or if you fail to meet reasonable standards of satisfactory performance, or to pass the training program required
        for your position.</p>

    <p>You also understand that during the probationary period, you shall not be entitled to any benefits from the Company.</p>

    <h3>Working Hours</h3>
    <p>Your official work hour is eight (8) hours per day excluding one hour lunch break, or forty-eight (48) hours per week.
        However, you are expected to put in longer and flexible working hours, as and when required, having regard to your
        position, the nature of your job and the requirements of the Company. The Management reserves the rights to adjust the
        working hours, as they deem necessary.</p>

    <h3>Transfer and Secondment</h3>
    <p>You agree that as an employee of the Company, whether on probationary or confirmed, you may be directed to transfer from
        one location to another or from any type of work to another. During the entire period of your employment, you may be
        assigned to any team, project, department, workplace, or any other branch of the Company for such periods as may be
        determined by the Company as reasonable and necessary to its operations.</p>

    <p>Company reserves the sole right to change your title, position and responsibilities from time to time as required by the
        business needs of the Company. You’ll be communicated to any change in your assignment.</p>

    <h3>Resignation and Termination</h3>
    <p>While serving your period of probation or extension thereof, your employment may be terminated by either party, serving
        on the other one (7) days written notice or one (7) days salary in lieu of such notice.</p>

    <p>Notwithstanding any of the other terms in this Agreement, and without prejudice to any of our rights generally, the
        Company reserves the right to at all times to terminate your appointment immediately by notice in writing to that effect
        and without any payment of salary in lieu of such notice or any compensation whatsoever for any of the following
        reasons:</p>
    <p style="margin-left:2%;">a. Your work performance is constantly poor and does not meet the normal requirement of the job</p>
    <p style="margin-left:2%;">b. You are found to be in demand or accept bribe, or any illegal gratification whatsoever</p>
    <p style="margin-left:2%;">c. You are found to have disclosed to any outside party the Company’s secret or confidential information</p>
    <p style="margin-left:2%;">d. You are found to engage or incite other to engage in illegal strikes or work slowdown</p>
    <p style="margin-left:2%;">e. You do not turn up for duty for more than two (2) continuous working days without prior leave or consent of the
        management</p>
    <p style="margin-left:2%;">f. You commit any act or thing which may bring discredit or disrepute on the Company</p>
    <p style="margin-left:2%;">g. You neglect or refuse, without reasonable cause, to attend to the business of the Company to which he is assigned
        duties</p>
    <p style="margin-left:2%;">h. You misappropriate assets of the Company</p>
    <p style="margin-left:2%;">i. You fail to observe and perform any of the duties and obligations imposed by this Agreement or which are imposed by
        law</p>
    <p style="margin-left:2%;">j. You act in breach of this Agreement</p>
    <p style="margin-left:2%;">k. You become of unsound mind</p>
    <p style="margin-left:2%;">l. You are guilty of dishonesty</p>
    <p style="margin-left:2%;">m. You are incapacitated by reason of his health or accident from performing his duties and obligations hereunder and
        shall have been so incapacitated for a total period of 180 days or more</p>
    <p>Upon termination of the Appointment for whatever reason, you shall:</p>

    <p style="margin-left:2%;">a. Deliver to the Company all books, documents, papers, materials, diskettes, tapes or other computer material,
        credit
        cards, and other property relating to the business of the Company which may then be in your possession or under your
        power of control</p>
    <p style="margin-left:2%;">b. You shall not, at any time after the termination of the Appointment for whatever reason, represent yourself as
        beingin any way connected with the business of the Company
    <p style="margin-left:2%;">c. If before the expiration of this Agreement, the Appointment hereunder shall be terminated by reason of the
        liquidation of the Company for the purposes of amalgamation or reconstruction or as part of any arrangement for the
        amalgamation of the undertaking of the Company not involving liquidation and you shall be offered employment with the
        amalgamated or reconstructed company on terms generally, not less favorable than the terms of this Agreement and you
        shall have no claim against the Company in respect of the termination of your Appointment by the Company</p>

    <h3>Rules of Conduct</h3>
    <p>The Company shall provide you with an Employee Handbook and you agree to abide by all the Company rules and policies
        (including those in any employee handbook) during the course of your employment.</p>

    <p>You agree to perform the duties and responsibilities assigned to you to the best of your ability. Failure to attain
        work
        goals within the allotted reasonable period, producing unsatisfactory results, resulting in a performance rating of
        below average of its equivalent for the program or department assigned to you under the Company’s performance appraisal
        system may result in termination from employment.</p>

    <p>You agree to refrain, during your employment with the Company, from engaging in any activity which is prejudicial to
        the
        interests of the Company or which will interfere with the performance of your job, whether during or outside Company
        hours, without the prior written consent of the Company. You agree to give immediate notice to the Company of any
        possible conflict of interest, which you may have.</p>

    <p>During your employment with the Company, you will not be engaged or concerned or be involved in the conduct of any
        activity or business which is similar to or competes with any activity carried on by the Company. Such engagement,
        concerning, or involvement with a competitor is prohibited regardless of whether the conduct is direct or indirect.</p>

    <p>Except as disclosed or declared to the Company in writing prior to the date of this Agreement, you shall not, until One
        (1) year after the termination of the Appointment:</p>
    <p style="margin-left:2%;">a. Within the relevant jurisdiction or marketing area in which you had been operating and/or doing business for and on
        behalf of the Company and/or any related company, directly or indirectly own, manage as a direct or indirect owner,
        operate, control, or participate in any way in the ownership or in the board decision making, or in any way directly or
        indirectly having the control of the board, of any business of the type and character which is competitive with the
        product lines carries and/or the services conducted by the Company and/or any related company as of the dare of
        termination of the Appointment</p>
    <p style="margin-left:2%;">b. Persuade or attempt to persuade any potential customer or client to which the Company or any related company has made
        a presentation, or with which the Company or any related company has been in negotiations or having discussions, not to
        deal with or hire the Company or any related company or to deal with or hire another company
    <p style="margin-left:2%;">c. Solicit for yourself or any person other than the Company or any related company the business of any principal or
        supplier of the Company or any related company, or was its principal or supplier within one (1) year prior to the date
        of termination of the Appointment</p>
    <p style="margin-left:2%;">d. Persuade or attempt to persuade any employee of the Company or any related company, or any individual who was an
        employee during the one (1) year prior to the date of termination of the Appointment, to leave the company or any
        related company’s employ</p>

    <p>You shall keep secret and shall not at any time, for whatever reason, (whether during the Appointment or after the
        termination of the Appointment) use your own or another’s advantage, or reveal to any person, firm or company, any of
        the trade secrets, business methods or information which you knew or ought reasonably to have known to be confidential
        concerning the business or affairs of the Company or any related company so far as they shall have come to your
        knowledge during the Appointment. The restrictions shall not apply to any disclosure or use authorized by the Directors
        or required by law or by the Appointment.</p>

    <p>The Company reviews its policies and procedures on a regular basis having regard to changes in applicable laws and
        business conditions. The Company reserved the right, in its absolute discretion, to supplement, change, discontinue or
        amend its policies and procedures at any time without consultation with you. The Company will communicate any changes to
        its policies and procedures to you as soon as it is practicable to do so.</p>
    <br>
    <h3>Personal Data Privacy</h3>
    <p>The company may conflict and retain personal data concerning you, may use such data for all matters connected to your
        Employment, and may pass such data to other Companies and/or to any regulatory body for any purpose connected with the
        employment.</p>

    <p>In accepting this employment, you hereby consent to the processing (including collection, use, and local and
        international transmission to databases within the Company or third-party contractors storing such data on Company’s
        behalf) of your personal data. You may request and obtain access to your own personal data (where collected), and
        correct or delete such data (where appropriate).</p>

    <h3>Verification</h3>
    <p>You agree, consent and allow the company to verify and check in your behalf, with the registrar of your schools any and
        all information pertinent to your educational background and information.</p>
    <h3>Governing Law</h3>
    <p>The terms and conditions of your employment, as well as this agreement, shall be governed by and construed in accordance
        with the laws of Malaysia. The Malaysian courts and labor arbitral agencies shall have jurisdiction over disputes, which
        may arise in connect with your employment.</p>

    <h3>Repealing Clause</h3>
    <p>It is expressly agreed and understood that there are no other agreements or understandings, verbal or written, between
        you and the Company or any of its agents and representatives apart from this agreement. Any alterations or revisions of
        the terms and conditions herein must be made in writing and executed by both you and the Company before such alterations
        or revisions may take effect.</p>

    <p>Your signature in the space provided will acknowledge your acceptance of these terms of employment.</p>

    <p>We are excited to have you as part of our Asia Fibre Solution team. We are confident that your employment will be
        beneficial to both parties.</p>

    <p>Yours Sincerely,</p>
    <br>
    <br>
    <br>
    <br>
    <p>___________________________</p>
    <p>Dennis New</p>
    <p>General Manager</p>

    <hr>
    <h4 style="text-align: center;">
        ACKNOWLEDGEMENT</h4>
    <p>I, ______________________________, NRIC _______________________, hereby acknowledge receipt of this Employment
        Contract and confirm that I have read and fully understood the contents and agree to accept the terms and
        conditions of my service with Asia Fibre Solutions Sdn. Bhd.</p>
    <p>Signature: <span style="margin-left: 10%;">Date:</span></p>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <h3 style="text-align: center;">
        Compensation Details</h3>
    <p style="font-weight: bold;">Name: {{$data->fullname}}</p>
    <p style="font-weight: bold;">NRIC: {{$data->nric}}</p>
    <p style="font-weight: bold;">Position: {{$data->position}}</p>

    <br>
    <table border="1">
        <tbody>
            <tr>
                <td>Item</td>
                <td>Per Month in RM</td>
                <td>Per Annum in RM</td>
            </tr>
            <tr>
                <td>Basic Salary</td>
                <td>{{$data->salary}}</td>
                <td>{{$data->salary * 12}}</td>
            </tr>
            <tr>
                <td>*Commission Scheme</td>
                <td>By meeting and exceeding company’s performance criteria</td>
                <td>By meeting and exceeding company’s performance criteria</td>
            </tr>
            <tr>
                <td>Medical Claim</td>
                <td>50.00</td>
                <td>500.00</td>
            </tr>
        </tbody>
    </table>
    <br>
    <h3>Statutory Benefits</h3>

    <p style="font-weight: bold; margin-left: 2%">1. Employee Provident Fund and/or SOCSO:</p>
    <p style="margin-left: 3%">You will be entitled to Employee Provident Fund and/or SOCSO
        in accordance with the statutory
        requirements</p>
    <p style="font-weight: bold; margin-left: 2%">2. Annual Leave</p>
    <p style="margin-left: 3%">Upon completion of your probationary period, you will be entitled to twelve (12) working days of paid
        annual leave per calendar year in addition to the paid holidays based on the gazette Public Holidays of
        the Federal Government and State Governments where you are located</p>
    <p style="font-weight: bold; margin-left: 2%">3. Medical Leave</p>
    <p style="margin-left: 3%">Upon completion of your probationary period, you will be entitled to fourteen (14) days medical leave
        per calendar year, which is subject to the presentation of medical certificate</p>

    <p style="font-weight: bold; margin-left: 2%">4. Maternity Leave</p>
    <p style="margin-left: 3%">Female employees are entitled for sixty (60) consecutive days of maternity leave at full pay, as long
        as
        you have worked for the Company for at least 90 days in the four months before starting maternity leave,
        and/or in accordance with the requirement of law</p>
    <p style="font-weight: bold; margin-left: 2%">5. Birthday Leave</p>
    <p style="margin-left: 3%">Upon completion of your probationary period, you will be entitled to one (1) day birthday leave per
        calendar year, which is subject to notice to your report manager and/or Human Resource Manager.</p>
    <p style="font-weight: bold; margin-left: 2%">6. Other Paid Leaves of Absence</p>
    <p style="margin-left: 3%">All other paid leaves of absence (e.g. Compassionate, examination, paternity, marriage, representing
        state
        or country leave, etc) is subject to the Company’s discretion and approval and will be granted in
        accordance with our policy at the time in force.</p>
    <br>
    <br>
    <br>
    <p>Yours Sincerely,</p>
    <br>
    <br>
    <br>
    <br>
    <p>_________________________</p>
    <p>Dennis New</p>
    <p>General Manager</p>
    <h3 style="text-align: center;">ACCEPTANCE FORM</h3>
    <p>I hereby understand and accept all the terms and conditions stated in this letter. In addition to that, I hereby
        declare that I do not have any life-threatening illnesses and/or medical condition, history of drug abuse and
        alcoholism. I understand that any false declaration made by me will automatically result in the immediate
        termination of this contract by the Company.</p>
    <br>
    <br>
    <br>
    <br>
    <p>_________________________</p>
    <p>Name and NRIC: {{$data->fullname}} ({{$data->nric}})</p>
    <p>Date: {{$data->effdate}}</p>
    <i>Please sign and return to your Supervisor promptly.</i>
</body>

</html>