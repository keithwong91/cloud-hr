@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><i class="fa fa-clock"></i> {{ __('Inquiry Result') }}</div>

                <div class="card-body">
                    <table id='leaveList' class="table table-hover table-bordered">
                        <thead>
                            <tr>
                                <th scope="col" style="font-weight: bold !important;">Name</th>
                                <th scope="col" style="font-weight: bold !important;">Type</th>
                                <th scope="col" style="font-weight: bold !important;">Create at</th>
                                <th scope="col" style="font-weight: bold !important;">Status</th>
                                <th scope="col" style="font-weight: bold !important;">Date</th>
                                <th scope="col" style="font-weight: bold !important;">Day(s)</th>
                                <th scope="col" style="font-weight: bold !important;">AM/PM</th>
                                <th scope="col" style="font-weight: bold !important;">Reason</th>
                                <th scope="col" style="font-weight: bold !important;">Attachment</th>
                                <th scope="col" style="font-weight: bold !important;">Remark</th>
                                <th scope="col" style="font-weight: bold !important;">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($leaveApplication as $la)
                            <tr>
                                <form action="{{route('approveUserLeave')}}" method="POST">
                                    @csrf
                                    <th scope="row">{{$la->name}}<input type="text" hidden class="form-control form-control-sm" value='{{$la->id}}' name="leave_id" placeholder="" /></th>
                                    <td>{{$la->leave_name}}</td>
                                    <td>{{$la->created_at}}</td>
                                    <td>
                                        @if($la->status == 0)
                                        <span class="pending">Pending</span>
                                        @elseif($la->status == 1)
                                        <span class="approved">Approved</span>
                                        @elseif($la->status == 2)
                                        <span class="decline">Decline</span>
                                        @elseif($la->status == 3)
                                        <span class="withdraw">Withdraw</span>
                                        @endif
                                    </td>
                                    <td><i class="fa fa-calendar-alt"></i> {{$la->from}} ⇨ {{$la->to}}</td>
                                    <td><i class="fa fa-clock"></i> {{$la->day}} Day(s)</td>
                                    <td>
                                        @if($la->daytime == 0)
                                        <span>Full Day</span>
                                        @elseif($la->daytime == 1)
                                        <span>AM</span>
                                        @elseif($la->daytime == 2)
                                        <span>PM</span>
                                        @endif
                                    </td>
                                    <td>{{$la->reason}}</td>
                                    @if($la->attachment_name)
                                    <td><a href="{{ URL::to('/') }}/attachment/{{$la->attachment}}" target="_blank"><i class="fa fa-search"></i> {{$la->attachment_name ?? 'No Attachment'}}</a></td>
                                    @else
                                    <td><i class="fa fa-search"></i> {{$la->attachment_name ?? 'No Attachment'}}</td>
                                    @endif
                                    <td><input type="text" class="form-control form-control-sm" name="remark" placeholder="" /></td>
                                    <td><button type="submit" onclick="return confirm('Are you sure?')" name='decline' class="btn btn-outline-danger btn-sm">Decline</button> <button type="submit" onclick="return confirm('Are you sure?')" name='approve' class="btn btn-success btn-sm">Approve</button></td>
                                </form>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="col-md-12 py-4">
            <div class="card">
                <div class="card-header"><i class="fa fa-clock"></i> {{ __('Approved Leave') }}</div>

                <div class="card-body">
                    <table id='approvedLeave' class="table table-hover table-bordered">
                        <thead>
                            <tr>
                                <th scope="col" style="font-weight: bold !important;">Name</th>
                                <th scope="col" style="font-weight: bold !important;">Type</th>
                                <th scope="col" style="font-weight: bold !important;">Create at</th>
                                <th scope="col" style="font-weight: bold !important;">Status</th>
                                <th scope="col" style="font-weight: bold !important;">Date</th>
                                <th scope="col" style="font-weight: bold !important;">Day(s)</th>
                                <th scope="col" style="font-weight: bold !important;">AM/PM</th>
                                <th scope="col" style="font-weight: bold !important;">Reason</th>
                                <th scope="col" style="font-weight: bold !important;">Attachment</th>
                                <th scope="col" style="font-weight: bold !important;">Remark</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($approvedleave as $la)
                            <tr>
                                <form action="{{route('approveUserLeave')}}" method="POST">
                                    @csrf
                                    <th scope="row">{{$la->name}}<input type="text" hidden class="form-control form-control-sm" value='{{$la->id}}' name="leave_id" placeholder="" /></th>
                                    <td>{{$la->leave_name}}</td>
                                    <td>{{$la->created_at}}</td>
                                    <td>
                                        @if($la->status == 0)
                                        <span class="pending">Pending</span>
                                        @elseif($la->status == 1)
                                        <span class="approved">Approved</span>
                                        @elseif($la->status == 2)
                                        <span class="decline">Decline</span>
                                        @elseif($la->status == 3)
                                        <span class="withdraw">Withdraw</span>
                                        @endif
                                    </td>
                                    <td><i class="fa fa-calendar-alt"></i> {{$la->from}} ⇨ {{$la->to}}</td>
                                    <td><i class="fa fa-clock"></i> {{$la->day}} Day(s)</td>
                                    <td>
                                        @if($la->daytime == 0)
                                        <span>Full Day</span>
                                        @elseif($la->daytime == 1)
                                        <span>AM</span>
                                        @elseif($la->daytime == 2)
                                        <span>PM</span>
                                        @endif
                                    </td>
                                    <td>{{$la->reason}}</td>
                                    @if($la->attachment_name)
                                    <td><a href="{{ URL::to('/') }}/attachment/{{$la->attachment}}" target="_blank"><i class="fa fa-search"></i> {{$la->attachment_name ?? 'No Attachment'}}</a></td>
                                    @else
                                    <td><i class="fa fa-search"></i> {{$la->attachment_name ?? 'No Attachment'}}</td>
                                    @endif
                                    <td>{{$la->remark}}</td>
                                </form>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

<script type="text/javascript">
    $(function() {
        $('#leaveList').DataTable();
        $('#approvedLeave').DataTable();

    });
</script>
@endsection