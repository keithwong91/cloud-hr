@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><i class="fa fa-clock"></i> {{ __('Leave Summary') }}</div>

                <div class="card-body">
                    <div class="row">
                        @foreach($leaves as $leave)
                        <div class="col-md-2">
                            <span>{{$leave->leave_name}}</span>
                            <br>
                            <small>{{$leave->taken ?? 0}} / {{$leave->day}} day(s)</small>
                   
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12 py-4">
            <div class="card">
                <div class="card-header"><i class="fa fa-edit"></i> {{ __('Apply Leave') }}</div>

                <div class="card-body">
                    <form action="{{route('applyLeaveNow')}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="form-group col-md-4">
                                <label for="leaveType">{{ __('Leave Type *') }}</label>
                                <select required class="form-control form-control-sm" name="leave_id">
                                    @foreach($leaves as $leave)
                                    <option value="{{$leave->leave_id}}">{{$leave->leave_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="daterange">{{ __('From Date - To Date *') }}</label>
                                <input required autocomplete="false" class="form-control form-control-sm" id="daterange" name="daterange" placeholder="">
                                <input type="text" hidden class="form-control form-control-sm" id="from" name="from" readonly placeholder="">
                                <input type="text" hidden class="form-control form-control-sm" id="to" name="to" readonly placeholder="">
                            </div>
                            <div class="form-group col-md-4">
                                <label for="ampm">{{ __('AM/PM') }}</label>
                                <select onchange="updateDay()" class="form-control form-control-sm" id='daytime' name="daytime">
                                    <option value="0">Full Day</option>
                                    <option value="1">AM</option>
                                    <option value="2">PM</option>
                                </select>
                            </div>
                        </div>
                        <div class="row py-4">
                            <div class="form-group col-md-4">
                                <label for="Reason">{{ __('Reason') }}</label>
                                <input required type="text" class="form-control form-control-sm" name="reason" placeholder="">
                            </div>
                            <div class="form-group col-md-4">
                                <label for="day">{{ __('Total Day') }}</label>
                                <p id="day_display">0</p>
                                <input hidden type="text" class="form-control form-control-sm" id='day' name="day" readonly placeholder="">
                            </div>
                            <div class="form-group col-md-4">
                                <label for="attachment">{{ __('Attachment') }}</label>
                                <input type="file" class="form-control form-control-sm" name="attachment" placeholder="" enctype="multipart/form-data" accept="application/pdf,image/jpeg,image/jpg,image/png">
                            </div>
                        </div>
                        <p>{{$message ?? ''}}</p>
                        <button disabled id="submit_btn" type="submit" class="btn btn-success  btn-sm"><i class="fa fa-upload" style="color: #fff"></i> Submit</button>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><i class="fa fa-clock"></i> {{ __('Inquiry Result') }}</div>

                <div class="card-body">
                    <table id="leaveApplicationList" class="table table-hover table-bordered">
                        <thead>
                            <tr>
                                <th scope="col" style="font-weight: bold !important;"># No.</th>
                                <th scope="col" style="font-weight: bold !important;">Type</th>
                                <th scope="col" style="font-weight: bold !important;">Create at</th>
                                <th scope="col" style="font-weight: bold !important;">Status</th>
                                <th scope="col" style="font-weight: bold !important;">Date</th>
                                <th scope="col" style="font-weight: bold !important;">Day(s)</th>
                                <th scope="col" style="font-weight: bold !important;">AM/PM</th>
                                <th scope="col" style="font-weight: bold !important;">Reason</th>
                                <th scope="col" style="font-weight: bold !important;">Attachment</th>
                                <th scope="col" style="font-weight: bold !important;">Remark</th>
                                <th scope="col" style="font-weight: bold !important;">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($leaveApplication as $la)

                            <tr>
                                <form action="{{route('withdrawLeave')}}" method="POST">
                                    @csrf
                                    <th scope="row">{{$la->id}} <input value="{{$la->id}}" hidden name='leave_id' /></th>
                                    <td>{{$la->leave_name}}</td>
                                    <td>{{$la->created_at}}</td>
                                    <td>
                                        @if($la->status == 0)
                                        <span class="pending">Pending</span>
                                        @elseif($la->status == 1)
                                        <span class="approved">Approved</span>
                                        @elseif($la->status == 2)
                                        <span class="decline">Decline</span>
                                        @elseif($la->status == 3)
                                        <span class="withdraw">Withdraw</span>
                                        @endif
                                    </td>
                                    <td><i class="fa fa-calendar-alt"></i> {{$la->from}} ⇨ {{$la->to}}</td>
                                    <td><i class="fa fa-clock"></i> {{$la->day}} Day(s)</td>
                                    <td>
                                        @if($la->daytime == 0)
                                        <span>Full Day</span>
                                        @elseif($la->daytime == 1)
                                        <span>AM</span>
                                        @elseif($la->daytime == 2)
                                        <span>PM</span>
                                        @endif
                                    </td>
                                    <td>{{$la->reason}}</td>
                                    @if($la->attachment_name)
                                    <td><a href="{{ URL::to('/') }}/attachment/{{$la->attachment}}" target="_blank"><i class="fa fa-search"></i> {{$la->attachment_name ?? 'No Attachment'}}</a></td>
                                    @else
                                    <td><i class="fa fa-search"></i> {{$la->attachment_name ?? 'No Attachment'}}</td>
                                    @endif
                                    <td>{{$la->remark ?? 'N/A'}}</td>
                                    <td><button {{($la->status != 0) ? 'disabled' : 0}} onclick="return confirm('Are you sure?')" type="submit" class="btn btn-outline-{{($la->status != 0) ? 'link' : 'danger'}} btn-sm">Withdraw</button></td>
                                </form>
                            </tr>

                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

<script type="text/javascript">
    $(function() {
        var nowDate = new Date();
        var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0);
        var table = $('#leaveApplicationList').DataTable({
            order: [
                [0, 'desc']
            ],
        });

        $('input[name="daterange"]').daterangepicker({
            opens: 'left',
            daysOfWeekHighlighted: "0,6",
            daysOfWeekDisabled: [0, 6]
        }, function(start, end, label) {
            document.getElementById("from").value = start.format('YYYY-MM-DD');
            document.getElementById("to").value = end.format('YYYY-MM-DD');
            let daytime = document.getElementById("daytime").value;
            let date_1 = new Date(document.getElementById("to").value);
            let date_2 = new Date(document.getElementById("from").value);
            let difference = date_1.getTime() - date_2.getTime();
            let totalDay = Math.ceil(difference / (1000 * 3600 * 24));
            totalDay = totalDay + 1;
            if (daytime != 0) {
                totalDay = totalDay / 2;
            }
            document.getElementById("day").value = totalDay;
            document.getElementById("day_display").innerHTML = totalDay;
            updateDay()
        });
    });

    function updateDay() {
        let daytime = document.getElementById("daytime").value;
        let date_1 = new Date(document.getElementById("to").value);
        let date_2 = new Date(document.getElementById("from").value);
        let difference = date_1.getTime() - date_2.getTime();
        let totalDay = Math.ceil(difference / (1000 * 3600 * 24));
        totalDay = totalDay + 1;
        if (daytime != 0) {
            totalDay = totalDay / 2;
        }
        document.getElementById("day").value = totalDay;
        document.getElementById("day_display").innerHTML = totalDay;
        if (totalDay > 0) {
            $("#submit_btn").prop("disabled", false);
        }
    }
</script>
@endsection