@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-4">
            <div class="card">
                <div class="card-header"><i class="fa fa-edit"></i> {{ __('Edit Company') }}</div>

                <div class="card-body">
                    <form action="{{route('createCompany')}}" method="POST">
                        @csrf
                        <div class="row">
                            <div class="form-group col-md-12">
                                <label for="name">{{ __('Company Name') }}</label>
                                <input required type="text" class="form-control form-control-sm" name="name" placeholder="">
                            </div>
                        </div>
                        <p>{{ $createCompanyMessage ?? '' }}</p>
                        <button type="submit" class="btn btn-success  btn-sm"><i class="fa fa-upload" style="color: #fff"></i> Create</button>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="card-header"><i class="fa fa-edit"></i> {{ __('Edit Department') }}</div>

                <div class="card-body">
                    <form action="{{route('createDepartment')}}" method="POST">
                        @csrf
                        <div class="row">
                            <div class="form-group col-md-12">
                                <label for="name">{{ __('Department Name') }}</label>
                                <input required type="text" class="form-control form-control-sm" name="name" placeholder="">
                            </div>
                        </div>
                        <p>{{ $createDepartmentMessage ?? ''}}</p>
                        <button type="submit" class="btn btn-success  btn-sm"><i class="fa fa-upload" style="color: #fff"></i> Create</button>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="card-header"><i class="fa fa-edit"></i> {{ __('Edit Postion') }}</div>

                <div class="card-body">
                    <form action="{{route('createPosition')}}" method="POST">
                        @csrf
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="name">{{ __('Position Name') }}</label>
                                <input required type="text" class="form-control form-control-sm" name="name" placeholder="">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="name">{{ __('Position Ranking') }}</label>
                                <select class="form-control form-control-sm" name="ranking">
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                                <option value="9">9</option>
                                <option value="10" selected>10</option>
                                </select>
                            </div>
                        </div>
                        <p>{{ $createPositionMessage ?? ''}}</p>
                        <button type="submit" class="btn btn-success  btn-sm"><i class="fa fa-upload" style="color: #fff"></i> Create</button>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-12 py-4">
            <div class="card">
                <div class="card-header"><i class="fa fa-download"></i> {{ __('Inquiry Result') }}</div>
                <div class="card-body">
                    <h5>Company</h5>
                    <table id="companyList" class="table table-hover table-bordered">
                        <thead>
                            <tr>
                                <th scope="col" style="font-weight: bold !important;width:10%">Record Id</th>
                                <th scope="col" style="font-weight: bold !important;width:30%">Company</th>
                                <th scope="col" style="font-weight: bold !important;">Create At</th>
                                <th scope="col" style="font-weight: bold !important;">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($companies as $company)
                                <tr>
                                    <td scope="row">{{$company->id}} <input name="id" hidden value="{{$company->id}}" /></td>
                                    <td scope="row">{{$company->name}}</td>
                                    <td scope="row">{{$company->created_at}}</td>
                                    <td><form action="{{route('deleteCompany')}}" method="POST">
                                        @csrf
                                        <input name="id" hidden value="{{$company->id}}" />
                                        <button type="submit" name="delete" onclick="return confirm('Are you sure?')" class="btn btn-outline-danger btn-sm">Delete</button>
                                    </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <br>
                    <hr>
                    <br>
                    <h5>Department</h5>
                    <table id='departmentList' class="table table-hover table-bordered">
                        <thead>
                            <tr>
                                <th scope="col" style="font-weight: bold !important;width:10%">Record Id</th>
                                <th scope="col" style="font-weight: bold !important;width:30%">Department</th>
                                <th scope="col" style="font-weight: bold !important;">Create At</th>
                                <th scope="col" style="font-weight: bold !important;">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($departments as $department)
                                <tr>
                                    <td scope="row">{{$department->id}} <input name="id" hidden value="{{$department->id}}" /></td>
                                    <td scope="row">{{$department->name}}</td>
                                    <td scope="row">{{$department->created_at}}</td>
                                    <td><form action="{{route('deleteCompany')}}" method="POST">
                                        @csrf
                                        <input name="id" hidden value="{{$department->id}}" />
                                        <button type="submit" name="delete" onclick="return confirm('Are you sure?')" class="btn btn-outline-danger btn-sm">Delete</button>
                                    </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <br>
                    <hr>
                    <br>
                    <h5>Position</h5>
                    <table id='positionList' class="table table-hover table-bordered">
                        <thead>
                            <tr>
                                <th scope="col" style="font-weight: bold !important;width:10%">Record Id</th>
                                <th scope="col" style="font-weight: bold !important;width:30%">Postion</th>
                                <th scope="col" style="font-weight: bold !important;width:30%">Ranking</th>
                                <th scope="col" style="font-weight: bold !important;">Create At</th>
                                <th scope="col" style="font-weight: bold !important;">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($positions as $position)
                            
                                
                                <tr>
                                    <td scope="row">{{$position->id}}</td>
                                    <td scope="row">{{$position->name}}</td>
                                    <form action="{{route('deleteCompany')}}" method="POST">
                                        @csrf
                                    <td scope="row">
                                        <select class="form-control form-control-sm" name="ranking">
                                            <option value="1" {{$position->ranking == 1 ? 'selected' : ''}}>1</option>
                                            <option value="2" {{$position->ranking == 2 ? 'selected' : ''}}>2</option>
                                            <option value="3" {{$position->ranking == 3 ? 'selected' : ''}}>3</option>
                                            <option value="4" {{$position->ranking == 4 ? 'selected' : ''}}>4</option>
                                            <option value="5" {{$position->ranking == 5 ? 'selected' : ''}}>5</option>
                                            <option value="6" {{$position->ranking == 6 ? 'selected' : ''}}>6</option>
                                            <option value="7" {{$position->ranking == 7 ? 'selected' : ''}}>7</option>
                                            <option value="8" {{$position->ranking == 8 ? 'selected' : ''}}>8</option>
                                            <option value="9" {{$position->ranking == 9 ? 'selected' : ''}}>9</option>
                                            <option value="10" {{$position->ranking == 10 ? 'selected' : ''}}>10</option>
                                            <option value="99" {{$position->ranking == 99 ? 'selected' : ''}}>99</option>
                                        </select>
                                    </td>
                                    <td scope="row">{{$position->created_at}}</td>
                                    <td>
                                        <input name="id" hidden value="{{$position->id}}" />
                                        <button type="submit" name="delete" onclick="return confirm('Are you sure?')" class="btn btn-outline-danger btn-sm">Delete</button>
                                        <button type="submit" name="update" value="1" onclick="return confirm('Are you sure?')" class="btn btn-outline-success btn-sm">Update</button>
                                    </form>
                                    </td>
                                </tr>
                            
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

<script type="text/javascript">
    $(function() {
        $('#companyList').DataTable();
        $('#departmentList').DataTable();
        $('#positionList').DataTable();
    });
</script>
@endsection