<!DOCTYPE html>
<html>

<head>
    <!-- <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;500;600&display=swap" rel="stylesheet"> -->
    <style>
        p {
            font-size: 12px;
        }
    </style>
</head>

<body>
    <h1 style="text-align: center; color:#6a99d0; border-top: #6a99d0 1px solid; border-bottom: #6a99d0 1px solid; padding-top:10px; padding-bottom:10px">
        CONTRACT FOR SERVICES</h1>
    <!-- <img src="{{URL::asset('/image/logo.png')}}" alt="profile Pic" height="200" width="200"> -->

    <h3 style="text-align: center;">GENERAL CONTRACT FOR SERVICES</h3>
    <p>This Contract for Services ("Contract") is made and entered into by the undersigned parties: <strong>ASIA FIBRE
            SOLUTIONS
            SDN. BHD.</strong> (known as the "Company") and <strong>{{$data->fullname}}</strong> (known as the
        "Contractor").</p>
    <p>In consideration of the promises, rights and obligations set forth below, the parties hereby agree as follows:
    </p>
    <p>The term of this Contract shall begin on <strong>{{$data->effdate}}</strong> and continue until further notice,
        unless
        terminated
        earlier as set forth in this Contract. The term of this Contract may be extended by mutual agreement between the
        parties.</p>
    <strong>2. Services </strong>
    <p>The Contractor will provide the following services:</p>
    <p>I. To generate sales and submit application with details and required documents.<br>
        II. To recruit and arrange interview for candidates.<br>
        III. To process all sales into correct portal.<br>
    </p>
    <p>The Contractor shall take direction from the Direct Superior or as directed by Company's Board of Directors.
        Additional services or amendments to the services described above may be agreed upon between the parties.</p>
    <strong>3. Compensation</strong>
    <p>Subject to providing the services as outlined above, the Contractor will be paid the sum of <strong>RM
            {{$data->salary}}</strong> per
        month based on fulfillment of services. The Company will be invoiced monthly, with payment due within 90
        business
        days of receipt of the invoice.</p>
    <strong>4. Relationship</strong>
    <p>The Contractor will provide the Contractor's services to the Company as an independent contractor and not as an
        employee.</p>
    <p>Accordingly:<br>
        · The Contractor agrees that the Company shall have no liability or responsibility for the withholding,
        collection or payment of any taxes, employment insurance premiums or KWSP/Socso contributions on any amounts
        paid by the Company to the Contractor or amounts paid by the Contractor to its employees or contractors.<br>
        · The Contractor agrees that as an independent contractor, the Contractor will not be qualified to
        participate in or to receive any employee benefits that the Company may extend to its employees.<br>
        · The Contractor is free to provide services to other clients, so long as such other clients are not in
        competition with the Company and so long as there is no interference with the Contractor's contractual
        obligations to the Company.<br>
        · The Contractor has no authority to and will not exercise or hold itself out as having any authority to
        enter into or conclude any contract or to undertake any commitment or obligation for, in the name of or on
        behalf of the Company.<br>
    </p>
    <strong>5. Confidentiality and Intellectual Property</strong>
    <p>The Contractor hereby acknowledges that it has read and agrees to be bound by the terms and conditions of the
        Company's confidentiality and proprietary information agreement attached hereto as Schedule "A" and which forms
        an
        integral part of this Contract. If the Contractor retains any employees or contractors of its own who will
        perform
        services hereunder, the Contractor shall ensure that such employees or contractors execute a Contract no less
        protective of the Company's intellectual property and confidential information than the attached agreement.</p>
    <p>The Contractor hereby represents and warrants to the Company that it is not party to any written or oral
        agreement
        with any third party that would restrict its ability to enter into this Contract or the Confidentiality and
        Proprietary Information Agreement or to perform the Contractor's obligations hereunder and that the Contractor
        will
        not, by providing services to the Company, breach any non-disclosure, proprietary rights, non-competition,
        non-solicitation or other covenant in favor of any third party.</p>
    <p>The Contractor hereby agrees that, during the term of this Contract and for two (2) year following the
        termination
        hereof, the Contractor will not (i) recruit, attempt to recruit or directly or indirectly participate in the
        recruitment of any Company employee or (ii) directly or indirectly solicit, attempt to solicit, canvass or
        interfere
        with any customer or supplier of the Company in a manner that conflicts with or interferes in the business of
        the
        Company as conducted with such customer or supplier.</p>
    <strong>6. Termination</strong>
    <p>The Contractor agrees that the Company may terminate this Contract at any time without notice or any further
        payment if
        the Contractor is in breach of any of the terms of this Contract.</p>
    <p>The Company may terminate this Contract at any time at its sole discretion, upon providing to the Contractor 7
        calendar
        days advance written notice of its intention to do so or payment of fees in lieu thereof.</p>
    <p>The Contractor may terminate this Contract at any time at its sole discretion upon providing to the Company 7
        calendar
        days advance written notice of Contractor's intention to do so or payment of fees in lieu thereof. Upon receipt
        of such
        notice the Company may waive notice in which event this Contract shall terminate immediately.</p>

    <strong>7. Obligations Surviving Termination of this Contract</strong>
    <p>All obligations to preserve the Company's Confidential Information, Intellectual Property and other warranties
        and representations set forth herein shall survive the termination of this Contract.</p>
    <strong>8. Entire Contract</strong>
    <p>This Contract, together with the Confidentiality and Proprietary Information Agreement, represents the entire
        agreement between the parties and the provisions of this Contract shall supersede all prior oral and written
        commitments, contracts and understandings with respect to the subject matter of this Contract. This Contract may
        be amended without prior notice to the contractor.</p>
    <strong>9. Assignment</strong>
    <p>This Contract shall inure to the benefit of and shall be binding upon each party's successors and assigns.
        Neither party shall assign any right or obligation hereunder in whole or in part, without the prior written
        consent of the other party.</p>
    <strong>10. Governing Law and Principles of Construction.</strong>
    <p>If any provision in this Contract is declared illegal or unenforceable, the provision will become void, leaving
        the remainder of this Contract in full force and effect.</p>
    <br><br>
    <p>IN WITNESS WHEREOF, the parties hereto have caused this Contract to be executed by their duly authorized
        representatives, effective as of the day and year first above written.</p>
    <table style="width: 680.453px;">
        <tbody>
            <tr>
                <td style="width: 320px;">By<br /><br /><br /></td>
                <td style="width: 357.453px;">By<br /><br /><br /></td>
            </tr>
            <tr>
                <td style="width: 320px;">Procurement Executive's Signature</td>
                <td style="width: 357.453px;">Contractor's Signature</td>
            </tr>
            <tr>
                <td style="width: 320px;">Name: Dennis New</td>
                <td style="width: 357.453px;">Name: {{$data->fullname}}<br />IC No: {{$data->nric}}</td>
            </tr>
            <tr>
                <td style="width: 320px;">Date: {{$data->effdate}}</td>
                <td style="width: 357.453px;">Date: {{$data->effdate}}</td>
            </tr>
        </tbody>
    </table>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <h5 style="text-align: center;">Schedule "A"</h5>
    <h3 style="text-align: center;">CONFIDENTIALITY AND PROPRIETARY INFORMATION AGREEMENT</h3>
    <p>In consideration of your engagement as an independent contractor with <strong>ASIA FIBRE SOLUTIONS SDN.
            BHD.</strong> (known as
        the "Company"), the undersigned (known as the "Contractor") agrees and covenants as follows:</p>
    <p>1. Engagement with the Company as an independent contractor ("Engagement") will give the Contractor access
        to
        proprietary and confidential information belonging to the Company, its customers, its suppliers and others
        (the proprietary and confidential information is collectively referred to in this Agreement as "Confidential
        Information"). Confidential Information includes but is not limited to customer lists, marketing plans,
        proposals, contracts, technical and/or financial information, databases, software and know-how. All
        Confidential Information remains the confidential and proprietary information of the Company.</p>
    <p>2. As referred to herein, the "Business of the Company" shall relate to the business of the
        Company as the same is determined by the Board of Directors of the Company from time to time.</p>
    <p>3. The Contractor may in the course of the Contractor's Engagement with the Company conceive, develop or
        contribute to material or information related to the Business of the Company, including, without limitation,
        software, technical documentation, ideas, inventions (whether or not patentable), hardware, know-how,
        marketing plans, designs, techniques, documentation and records, regardless of the form or media, if any, on
        which such is stored (referred to in this Agreement as "Proprietary Property"). The Company shall exclusively
        own, and the Contractor does hereby assign to the Company, all Proprietary Property which the Contractor
        conceives, develops or contributes to in the course of the Contractor's Engagement with the Company and all
        intellectual and industrial property and other rights of any kind in or relating to the Proprietary Property,
        including but not limited to all copyright, patent, trade secret and trade-mark rights in or relating to the
        Proprietary Property. Material or information conceived, developed or contributed to by the Contractor
        outside work hours on the Company's premises or through the use of the Company's property and/or assets shall
        also be Proprietary Property and be governed by this Agreement if such material or information relates to the
        Business of the Company. The Contractor shall keep full and accurate records accessible at all times to the
        Company relating to all Proprietary Property and shall promptly disclose and deliver to the Company all
        Proprietary Property.</p>
    <p>4. The Contractor shall, both during and after the Contractor's Engagement with the Company, keep all
        Confidential Information and Proprietary Property confidential and shall not use any of it except for the
        purpose of carrying out authorized activities on behalf of the Company. The Contractor may, however, use or
        disclose Confidential Information which:</p>
    <p>i. is or becomes public other than through a breach of this Agreement;</p>
    <p>ii. is required to be disclosed by law, whether under an order of a court or government tribunal or other
        legal process, provided that Contractor informs the Company of such requirement in sufficient time to allow
        the Company to avoid such disclosure by the Contractor.</p>
    <p>iii. The Contractor shall return or destroy, as directed by the Company, Confidential Information, Proprietary
        Property and any other Company property to the Company upon request by the Company at any time. The
        Contractor shall certify, by way of affidavit or statutory declaration, that all such Confidential
        Information, Proprietary Property or Company property has been returned or destroyed, as applicable.</p>
    <p>5. The Contractor covenants and agrees not to make any unauthorized use whatsoever of or to bring onto the
        Company's
        premises for the purpose of making any unauthorized use whatsoever of any trade secrets, confidential
        information or
        proprietary property of any third party, including without limitation any trade-marks or copyrighted materials,
        during
        the course of the Contractor's Engagement with the Company.</p>

    <p>6. At the reasonable request and at the sole expense of the Company, the Contractor shall do all reasonable acts
        necessary and sign all reasonable documentation necessary in order to ensure the Company's ownership of the
        Proprietary
        Property, the Company property and all intellectual and industrial property rights and other rights in the same,
        including but not limited to providing to the Company written assignments of all rights to the Company and any
        other
        documents required to enable the Company to document rights to and/or register patents, copyrights, trade-marks,
        industrial designs and such other protections as the Company considers advisable anywhere in the world.</p>

    <p>7. The Contractor hereby irrevocably and unconditionally waives all moral rights the Contractor may now or in the
        future have in any Proprietary Property.</p>

    <p>8. The Contractor agrees that the Contractor will, if requested from time to time by the Company, execute such
        further reasonable agreements as to confidentiality and proprietary rights as the Company's customers or
        suppliers reasonably
        require to protect confidential information or proprietary property.</p>
    <p>9. Regardless of any changes in position, fees or otherwise, including, without limitation, termination of the
        Contractor's Engagement with the Company, unless otherwise stipulated pursuant to the terms hereof, the
        Contractor will
        continue to be subject to each of the terms and conditions of this Agreement and any other(s) executed pursuant
        to the
        preceding paragraph.</p>
    <p>10. The Contractor agrees that the Contractor's sole and exclusive remedy for any breach by the Company of this
        Agreement will be limited to monetary damages and in case of any breach by the Company of this Agreement or any
        other
        Agreement between the Contractor and the Company, the Contractor will not make any claim in respect of any
        rights to or
        interest in any Confidential Information or Proprietary Property.</p>
    <p>11. The Contractor acknowledges that the services provided by the Contractor to the Company under this Agreement
        are
        unique. The Contractor further agrees that irreparable harm will be suffered by the Company in the event of the
        Contractor's breach or threatened breach of any of his or her obligations under this Agreement, and that the
        Company
        will be entitled to seek, in addition to any other rights and remedies that it may have at law or equity, a
        temporary or
        permanent injunction restraining the Contractor from engaging in or continuing any such breach hereof. Any
        claims
        asserted by the Contractor against the Company shall not constitute a defense in any injunction action,
        application or
        motion brought against the Contractor by the Company.</p>
    <p>12. This Agreement is governed by the laws of Malaysia the parties agree to the non-exclusive jurisdiction of the
        courts in relation to this Agreement.</p>

    <p>13. If any provision of this Agreement is held by a court of competent jurisdiction to be invalid or
        unenforceable,
        that provision shall be deleted and the other provisions shall remain in effect.</p>
    <br>
    <br>
    <p> IN WITNESS WHEREOF the Company and the Contractor have caused this Contract to be executed effective as of the
        day and year first above written.</p>
    <table style="width: 680.453px;">
        <tbody>
            <tr>
                <td style="width: 320px;">By<br /><br /><br /></td>
                <td style="width: 357.453px;">By<br /><br /><br /></td>
            </tr>
            <tr>
                <td style="width: 320px;">Procurement Executive's Signature</td>
                <td style="width: 357.453px;">Contractor's Signature</td>
            </tr>
            <tr>
                <td style="width: 320px;">Name: Dennis New</td>
                <td style="width: 357.453px;">Name: {{$data->fullname}}<br />IC No: {{$data->nric}}</td>
            </tr>
            <tr>
                <td style="width: 320px;">Date: {{$data->effdate}}</td>
                <td style="width: 357.453px;">Date: {{$data->effdate}}</td>
            </tr>
        </tbody>
    </table>

</body>

</html>