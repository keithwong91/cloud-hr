@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><i class="fa fa-edit"></i> {{ __('Edit Leave') }}</div>

                <div class="card-body">
                    <form action="{{route('addLeaveType')}}" method="POST">
                        @csrf
                        <div class="row">
                            <div class="form-group col-md-4">
                                <label for="leaveName">{{ __('Leave Name') }}</label>
                                <input required type="text" class="form-control form-control-sm" name="leaveName" placeholder="">
                            </div>
                            <div class="form-group col-md-4">
                                <label for="totalDay">{{ __('Total Day') }}</label>
                                <input required type="number" class="form-control form-control-sm" name="totalDay" placeholder="">
                            </div>
                        </div>
                        <div class="form-group col-md-4 py-4">
                        <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="no_prorate" id="no_prorate">
                                <label class="form-check-label" for="no_prorate">
                                    {{ __('No prorate needed') }}
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="permenantOnly" id="permenantOnly">
                                <label class="form-check-label" for="permenantOnly">
                                    {{ __('For permenant only') }}
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="applyForAll" id="applyForAll">
                                <label class="form-check-label" for="applyForAll">
                                    {{ __('Apply for all company') }}
                                </label>
                            </div>
                        </div>
                        <p>{{$addLeaveTypeMsg ?? ''}}</p>
                        <button type="submit" class="btn btn-success  btn-sm"><i class="fa fa-upload" style="color: #fff"></i> Create</button>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-12 py-4">
            <div class="card">
                <div class="card-header"><i class="fa fa-edit"></i> {{ __('Add Leave To Company') }}</div>

                <div class="card-body">
                    <form action="{{route('updateCompanyLeave')}}" method="POST">
                        @csrf
                        <div class="row">
                            <div class="form-group col-md-4">
                                <label for="company">{{ __('Company Name') }}</label>
                                <select required class="form-control form-control-sm" name="company">
                                    @foreach($companies as $company)
                                    <option value="{{$company->id}}">{{$company->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="leaveType">{{ __('Leave Type') }}</label>
                                <select required class="form-control form-control-sm" name="leaveType">
                                    @foreach($leaves as $leave)
                                    <option value="{{$leave->id}}">{{$leave->leave_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="leaveType">{{ __('Total Day') }}</label>
                                <input required type="number" class="form-control form-control-sm" name="day" placeholder="">
                            </div>
                        </div>
                        <p></p>
                        <button type="submit" class="btn btn-success  btn-sm"><i class="fa fa-upload" style="color: #fff"></i>Update / Add Now</button>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><i class="fa fa-download"></i> {{ __('Inquiry Result') }}</div>
                <div class="card-body">
                    <table id='leaveList' class="table table-hover table-bordered">
                        <thead>
                            <tr>
                                <th scope="col" style="font-weight: bold !important;width:10%">Record Id</th>
                                <th scope="col" style="font-weight: bold !important;width:20%">Leave Name</th>
                                <th scope="col" style="font-weight: bold !important;width:20%">Leave Default Day</th>
                                <th scope="col" style="font-weight: bold !important;width:10%">No prorate needed</th>
                                <th scope="col" style="font-weight: bold !important;width:15%">Change prorate needed</th>
                                <th scope="col" style="font-weight: bold !important;">Create At</th>
                                <th scope="col" style="font-weight: bold !important;">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($leaves as $leave)
                            <form action="{{route('deleteLeave')}}" method="POST">
                                @csrf
                                <tr>
                                    <td scope="row">{{$leave->id}} <input name="id" hidden value="{{$leave->id}}" /></td>
                                    <td scope="row">{{$leave->leave_name}}</td>
                                    <td scope="row">{{$leave->day}}</td>
                                    <td scope="row">{{$leave->no_prorate ? '✅' : '❌'}}</td>
                                    <td scope="row"><a onclick="return confirm(' Are you sure?')" href="{{ route('changeProrateStatus',['id' => $leave->id]) }}" class="btn btn-success btn-sm">change to {{$leave->no_prorate ? 'No' : 'Yes'}}</a></td>
                                    <td scope="row">{{$leave->created_at}}</td>
                                    <td>
                                        <button type="submit" onclick="return confirm(' Are you sure?')" class="btn btn-outline-danger btn-sm">Delete</button>
                                    </td>
                                </tr>
                            </form>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-12 py-4">
            <div class="card">
                <div class="card-header"><i class="fa fa-download"></i> {{ __('Inquiry Result') }}</div>

                <div class="card-body">

                    <table id='companyLeaveList' class="table table-hover table-bordered">
                        <thead>
                            <tr>
                                <th scope="col" style="font-weight: bold !important;">Record Id</th>
                                <th scope="col" style="font-weight: bold !important;">Company</th>
                                <th scope="col" style="font-weight: bold !important;">Leave Type</th>
                                <th scope="col" style="font-weight: bold !important;">Total Day</th>
                                <th scope="col" style="font-weight: bold !important;">Created By</th>
                                <th scope="col" style="font-weight: bold !important;">Created At</th>
                                <th scope="col" style="font-weight: bold !important;">Updated At</th>
                                <th scope="col" style="font-weight: bold !important;">For Permenant Only</th>
                                <th scope="col" style="font-weight: bold !important;">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($companyLeaves as $companyLeave)
                            <form action="{{route('deleteCompanyLeave')}}" method="POST">
                                @csrf
                                <tr>
                                    <td scope="row">{{$companyLeave->id}} <input name="id" hidden value="{{$companyLeave->id}}" /></td>
                                    <th scope="row">{{$companyLeave->name}}</th>
                                    <td>{{$companyLeave->leave_name}}</td>
                                    <td>{{$companyLeave->day}}</td>
                                    <td>System</td>
                                    <td>{{$companyLeave->created_at}}</td>
                                    <td>{{$companyLeave->updated_at}}</td>
                                    <td>{{$companyLeave->is_for_permanent_only == true ? 'Yes' : 'No'}}</td>
                                    <td>
                                        <button type="submit" onclick="return confirm(' Are you sure?')" class="btn btn-outline-danger btn-sm">Delete</button>
                        </td>
                        </tr>
                        </form>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

<script type="text/javascript">
    $(function() {
        $('#leaveList').DataTable();
        $('#companyLeaveList').DataTable();
    });
</script>
@endsection