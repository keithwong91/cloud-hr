@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><i class="fa fa-upload"></i> {{ __('Upload Payroll') }}</div>

                <div class="card-body">
                    <form action="{{route('uploadcsv')}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="form-group col-md-4">
                                <label for="excel">{{ __('Excel file') }}</label>
                                <input type="file" class="form-control form-control-sm" name="excel" placeholder="" required>
                            </div>
                        </div>
                        <p></p>
                        <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-upload" style="color: #fff"></i> Submit</button>
                    </form>
                </div>
            </div>
        </div>
        
        <div class="col-md-12 py-4">
            <div class="card">
                <div class="card-header"><i class="fa fa-download"></i> {{ __('Inquiry Result') }}</div>

                <div class="card-body">

                    <table id='payrollList' class="table table-hover table-bordered">
                        <thead>
                            <tr>
                                <th scope="col" style="font-weight: bold !important;">Name</th>
                                <th scope="col" style="font-weight: bold !important;">NRIC</th>
                                <th scope="col" style="font-weight: bold !important;">Basic</th>
                                <th scope="col" style="font-weight: bold !important;">NPL</th>
                                <th scope="col" style="font-weight: bold !important;">Add Pay</th>
                                <th scope="col" style="font-weight: bold !important;">OT</th>
                                <th scope="col" style="font-weight: bold !important;">Bonus</th>
                                <th scope="col" style="font-weight: bold !important;">Meal</th>
                                <th scope="col" style="font-weight: bold !important;">Claims</th>
                                <th scope="col" style="font-weight: bold !important;">Inc 1</th>
                                <th scope="col" style="font-weight: bold !important;">Inc 2</th>
                                <th scope="col" style="font-weight: bold !important;">Attn Allow</th>
                                <th scope="col" style="font-weight: bold !important;">Plencash</th>
                                <th scope="col" style="font-weight: bold !important;">Projallow</th>
                                <th scope="col" style="font-weight: bold !important;">Cnyspcom</th>
                                <th scope="col" style="font-weight: bold !important;">Back pay</th>
                                <th scope="col" style="font-weight: bold !important;">Gross Pay</th>
                                <th scope="col" style="font-weight: bold !important;">EPF</th>
                                <th scope="col" style="font-weight: bold !important;">Socso</th>
                                <th scope="col" style="font-weight: bold !important;">EIS</th>
                                <th scope="col" style="font-weight: bold !important;">PCB</th>
                                <th scope="col" style="font-weight: bold !important;">Hostel</th>
                                <th scope="col" style="font-weight: bold !important;">Nett Pay</th>
                                <th scope="col" style="font-weight: bold !important;">Repf</th>
                                <th scope="col" style="font-weight: bold !important;">Rsocso</th>
                                <th scope="col" style="font-weight: bold !important;">Reis</th>
                                <th scope="col" style="font-weight: bold !important;">Dispute</th>
                                <th scope="col" style="font-weight: bold !important;">Referral</th>
                                <th scope="col" style="font-weight: bold !important;">Month / Year</th>
                                <th scope="col" style="font-weight: bold !important;">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($payroll as $pr)
                            <tr>
                                <th scope="row">{{$pr->empname}}</th>
                                <th scope="row">{{$pr->empnirc}}</th>
                                <th scope="row">{{$pr->basic}}</th>
                                <th scope="row">{{$pr->npl}}</th>
                                <th scope="row">{{$pr->addpay}}</th>
                                <th scope="row">{{$pr->ot}}</th>
                                <th scope="row">{{$pr->bonus}}</th>
                                <th scope="row">{{$pr->meal}}</th>
                                <th scope="row">{{$pr->claims}}</th>
                                <th scope="row">{{$pr->inc1}}</th>
                                <th scope="row">{{$pr->other2}}</th>
                                <th scope="row">{{$pr->other3}}</th>
                                <th scope="row">{{$pr->plencash}}</th>
                                <th scope="row">{{$pr->projallow}}</th>
                                <th scope="row">{{$pr->cnyspcom}}</th>
                                <th scope="row">{{$pr->other4}}</th>
                                <th scope="row">{{$pr->grosspay}}</th>
                                <th scope="row">{{$pr->empepf}}</th>
                                <th scope="row">{{$pr->empsocso}}</th>
                                <th scope="row">{{$pr->empeis}}</th>
                                <th scope="row">{{$pr->emppcb}}</th>
                                <th scope="row">{{$pr->hostel}}</th>
                                <th scope="row">{{$pr->nettpay}}</th>
                                <th scope="row">{{$pr->repf}}</th>
                                <th scope="row">{{$pr->rsocso}}</th>
                                <th scope="row">{{$pr->reis}}</th>
                                <th scope="row">{{$pr->dispute}}</th>
                                <th scope="row">{{$pr->referral}}</th>
                                <th scope="row">{{$pr->month}} / {{$pr->year}}</th>

                                <td><a onclick="return confirm('Are you sure?')" href="{{ route('deletePayroll',['id' => $pr->id]) }}" type="button" class="btn btn-outline-danger btn-sm">Delete</a></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

<script type="text/javascript">
    $(function() {
        // $('#payrollList').DataTable(order: [
        //         [0, 'desc']
        //     ],
        // );
        var table = $('#payrollList').DataTable({
            order: [
                [9, 'desc']
            ],
        });
    });
</script>
@endsection