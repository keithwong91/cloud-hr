@extends('layouts.app')

@section('content')
<div class="container-fluid">
    @if ($showPayslipApproval == true)
        <div class="col-md-12" style="text-align:right"><a class="btn btn-success mb-1" href="{{ route('approvalDownloadPayrollLeader') }}">Approve your staff payslip</a></div>
    @endif
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><i class="fa fa-download"></i> {{ __('Inquiry Result') }}</div>

                <div class="card-body">

                    <table id="payrollList" class="table table-hover table-bordered">
                        <thead>
                            <tr>
                            <th scope="col" style="font-weight: bold !important;">Name</th>
                                <th scope="col" style="font-weight: bold !important;">NRIC</th>
                                <th scope="col" style="font-weight: bold !important;">Type</th>
                                <th scope="col" style="font-weight: bold !important;">Year</th>
                                <th scope="col" style="font-weight: bold !important;">Month</th>
                                <th scope="col" style="font-weight: bold !important;">Basic</th>
                                <th scope="col" style="font-weight: bold !important;">EPF</th>
                                <th scope="col" style="font-weight: bold !important;">PCB</th>
                                <th scope="col" style="font-weight: bold !important;">Gross Pay</th>
                                <th scope="col" style="font-weight: bold !important;">Nett Pay</th>
                                <th scope="col" style="font-weight: bold !important;">Created at</th>
                                <th scope="col" style="font-weight: bold !important;">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($payroll as $pr)
                            <tr>
                                <th scope="row">{{$pr->empname}}</th>
                                <th scope="row">{{$pr->empnirc}}</th>
                                <th scope="row">{{$pr->other5 == 2.00 ? 'Payment voucher' : 'Payslip'}}</th>
                                <th scope="row">{{$pr->year}}</th>
                                <th scope="row">{{$pr->month}}</th>
                                <th scope="row">RM {{$pr->request != 2 ? '-' : $pr->basic}}</th>
                                <th scope="row">RM {{$pr->request != 2 ? '-' : $pr->empepf}}</th>
                                <th scope="row">RM {{$pr->request != 2 ? '-' : $pr->emppcb}}</th>
                                <th scope="row">RM {{$pr->request != 2 ? '-' : $pr->grosspay}}</th>
                                <th scope="row">RM {{$pr->request != 2 ? '-' : $pr->nettpay}}</th>
                                <th scope="row">{{$pr->created_at}}</th>

                                <td>
                                    @if ($pr->request == 0)
                                        <a type="button" href="{{route('requestPayslip',['id' => $pr->id]) }}" class="btn btn-success btn-outline btn-sm">Request</a>
                                    @endif
                                    @if ($pr->request == 1)
                                        <a type="button" href="{{route('cancelRequestPayslip',['id' => $pr->id]) }}" class="btn btn-danger btn-outline btn-sm">Cancel Request</a>
                                    @endif
                                    @if ($pr->request == 2)
                                        <a type="button" href="{{route('generatePayslip',['id' => $pr->id]) }}" class="btn btn-success btn-sm">Download</a>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

<script type="text/javascript">
    $(function() {
        $('#payrollList').DataTable();
    });
</script>
@endsection