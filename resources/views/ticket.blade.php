@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><i class="fa fa-download"></i> {{ __('Raised Ticket') }}</div>

                <div class="card-body">

                    <table id="payrollList" class="table table-hover table-bordered">
                        <thead>
                            <tr>
                                <th scope="col" style="font-weight: bold !important; width:10%"># Serial No.</th>
                                <th scope="col" style="font-weight: bold !important;">Content</th>
                                <th scope="col" style="font-weight: bold !important;">Raised By</th>
                                <th scope="col" style="font-weight: bold !important; width:10%">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($tickets as $ticket)
                            <form action="{{route('closeTicket2')}}" method="POST">
                                @csrf
                                <tr>
                                    <th scope="row">{{$ticket->id}}<input name='id' hidden value="{{$ticket->id}}"></th>
                                    <td>{{$ticket->content}}</td>
                                    <td>{{$ticket->name}}</td>
                                    <td><button onclick="return confirm('Are you sure?')" type="submit" class="btn btn-outline-success btn-sm">Close</button></td>
                                </tr>
                            </form>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

<script type="text/javascript">
    $(function() {
        $('#payrollList').DataTable();
    });
</script>
@endsection