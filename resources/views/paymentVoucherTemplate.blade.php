<!DOCTYPE html>
<html>

<head>
    <!-- <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;500;600&display=swap" rel="stylesheet"> -->
    <style>
        p {
            font-size: 14px;
            font-family: 'Courier New', Courier, monospace;
        }

        .main {
            width: 100%;
            height: 100px;
        }

        .left{
            width: 100%;
            margin-bottom: 20px;
        }
    </style>
</head>

<body>
    <h3 style="text-align: center;">{{$data->company}}</h3>
    <p>I/C # : {{$data->empnirc}}</p>
    <p>CREATED AT # : {{$data->created_at}}</p>
    <p>PRINT ON # : {{$data->print_on}}</p>
    <p>PAYMENT VOUCHER NO # : {{$data->serialno}}</p>
    <h3>PAYMENT VOUCHER</h3>
    <hr>
    <p>PAY TO : {{$data->empname}} ({{$data->empnirc}})</p>
    <p>PAY FOR : SERVICE-FEE-{{$data->month}}/{{$data->year}}</p>
    <p>PAYMENT MODE : ONLINE</p>
    <p>AMOUNT : RM {{$data->grosspay}} ONLY</p>
    <hr>
    <p>This is a computer-generated document. No signature is required.</p>
</body>

</html>