@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><i class="fa fa-users"></i> {{ __('Employee') }}</div>

                <div class="card-body">
                    <table id="profileList" class="table table-hover table-bordered">
                        <thead>
                            <tr>
                                <th scope="col" style="font-weight: bold !important; width: 5%">#</th>
                                <th scope="col" style="font-weight: bold !important;">Nric</th>
                                <th scope="col" style="font-weight: bold !important;">Name</th>
                                <th scope="col" style="font-weight: bold !important;">Email</th>
                                <th scope="col" style="font-weight: bold !important;">Phone</th>
                                <th scope="col" style="font-weight: bold !important;">Date of Birth</th>
                                <th scope="col" style="font-weight: bold !important;">Is admin</th>
                                <th scope="col" style="font-weight: bold !important;">Print Document</th>
                                <th scope="col" style="font-weight: bold !important;">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($profiles as $profile)
                            <tr>
                                <th scope="row">{{$profile->user_id}}</th>
                                <th scope="row">{{$profile->nric}}</th>
                                <th scope="row">{{$profile->name}}</th>
                                <th scope="row">{{$profile->email}}</th>
                                <th scope="row">{{$profile->phone}}</th>
                                <th scope="row">{{$profile->dob}}</th>
                                <th scope="row">{{$profile->is_admin == 1 ? '✅ yes' : '❎ no'}}</th>
                                <td style="width:15%">
                                    <a type="button" {{($profile->name == null) ? 'hidden' : ''}} href="{{route('contractPDF',['id' => $profile->user_id]) }}" class="btn btn-primary btn-sm">Contract</a>
                                    <a type="button" {{($profile->address_1 == null || $profile->company_id == null || $profile->salary == null) ? 'hidden' : ''}} href="{{route('confirmationPDF',['id' => $profile->user_id]) }}" href="" class="btn btn-primary btn-sm">Confirmation</a>
                                </td>
                                <td style="width:15%"><a href="{{ route('viewprofile',['id' => $profile->user_id]) }}" type="button" class="btn btn-outline-success btn-sm">View</a>
                                    <a onclick="return confirm('Are you sure?')" type="button" href="{{ route('disableProfile',['id' => $profile->user_id]) }}" class="btn btn-outline-danger btn-sm">Disable</a>
                                    <a onclick="return confirm('Are you sure?')" type="button" href="{{ route('setAsAdmin',['id' => $profile->user_id]) }}" class="btn btn-outline-info btn-sm">Set to admin</a>
                                    <a onclick="return confirm('Are you sure?')" type="button" href="{{ route('resetPassword',['id' => $profile->user_id]) }}" class="btn btn-outline-info btn-sm">Reset Password</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

<script type="text/javascript">
    $(function() {
        $('#profileList').DataTable();
    });
</script>

@endsection