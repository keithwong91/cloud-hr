@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><i class="fa fa-clock"></i> {{ __('Leave Summary') }}</div>
                <div class="card-body">
                    <div class="row">
                        @foreach($leaves as $leave)
                        <div class="col-md-2">
                            <span>{{$leave->leave_name}}</span>
                            <br>
                            <small>{{$leave->taken ?? 0}} / {{$leave->day}} day(s)</small>
                   
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-12 py-4">
            <div class="card">
                <div class="card-header"><i class="fa fa-user"></i> {{ __('Profile') }}</div>
                <div class="card-body">
                    <form action="{{route('updateProfile')}}" method="POST">
                        @csrf
                        <div class="row">
                            <div class="form-group col-md-4">
                                <label for="user_id">{{ __('User ID') }}</label>
                                <input readonly disabled type="text" required class="form-control form-control-sm" value="{{ Auth::user()->id }}" placeholder="">
                            </div>
                            <div class="form-group col-md-4">
                                <label for="name">{{ __('Name') }}</label>
                                <input type="text" required class="form-control form-control-sm" name="name" value="{{ $profile->name ?? '' }}" placeholder="">
                            </div>
                            <div class="form-group col-md-4">
                                <label for="email">{{ __('Email') }}</label>
                                <input type="email" required class="form-control form-control-sm" name="email" value="{{ $profile->email ?? '' }}" placeholder="">
                            </div>
                        </div>
                        <div class="row py-3">
                            <div class="form-group col-md-4">
                                <label for="phone">{{ __('Phone') }}</label>
                                <input type="text" required class="form-control form-control-sm" name="phone" value="{{ $profile->phone ?? '' }}" placeholder="">
                            </div>
                            <div class="form-group col-md-4">
                                <label for="dob">{{ __('Date of birth') }}</label>
                                <input type="date" required class="form-control form-control-sm" name="dob" value="{{ $profile->dob ?? '' }}" placeholder="">
                            </div>
                            <div class="form-group col-md-4">
                                <label for="nric">{{ __('NRIC') }}</label>
                                <input readonly disabled type="text" required class="form-control form-control-sm" name="nric" value="{{ Auth::user()->user_id }}" placeholder="">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-4">
                                <label for="address_1">{{ __('Address 1') }}</label>
                                <input type="text" required class="form-control form-control-sm" name="address_1" value="{{ $profile->address_1 ?? '' }}" placeholder="">
                            </div>
                            <div class="form-group col-md-4">
                                <label for="address_2">{{ __('Address 2') }}</label>
                                <input type="text" class="form-control form-control-sm" name="address_2" value="{{ $profile->address_2 ?? '' }}" placeholder="">
                            </div>
                            <div class="form-group col-md-4">
                                <label for="address_3">{{ __('Address 3') }}</label>
                                <input type="text" class="form-control form-control-sm" name="address_3" value="{{ $profile->address_3 ?? '' }}" placeholder="">
                            </div>
                        </div>
                        <div class="row py-3">
                            <div class="form-group col-md-4">
                                <label for="postcode">{{ __('Postcode') }}</label>
                                <input type="text" required class="form-control form-control-sm" name="postcode" value="{{ $profile->postcode ?? '' }}" placeholder="">
                            </div>
                            <div class="form-group col-md-4">
                                <label for="state">{{ __('State') }}</label>
                                <input type="text" required class="form-control form-control-sm" name="state" value="{{ $profile->state ?? '' }}" placeholder="">
                            </div>
                            <div class="form-group col-md-4">
                                <label for="country">{{ __('Country') }}</label>
                                <input type="text" required class="form-control form-control-sm" name="country" value="{{ $profile->country ?? '' }}" placeholder="">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-4">
                                <label for="company_id">{{ __('Company') }}</label>
                                <select disabled readonly class="form-control form-control-sm" name="company_id">
                                    <option value="null" {{ ($profile->company_id) == null ? 'selected' : '' }}>N/A</option>
                                    @foreach($companies as $company)
                                    <option value="{{ $company->id }}" {{ ($profile->company_id) == ($company->id) ? 'selected' : '' }}>{{ $company->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="grade_id">{{ __('Grade') }}</label>
                                <select disabled readonly class="form-control form-control-sm" name="grade_id">
                                    <option value="null" {{ ($profile->grade_id) == null ? 'selected' : '' }}>N/A</option>
                                    @foreach($positions as $position)
                                    <option value="{{ $position->id }}" {{ ($profile->grade_id) == ($position->id) ? 'selected' : '' }}>{{ $position->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="department_id">{{ __('Department') }}</label>
                                <select disabled readonly class="form-control form-control-sm" name="department_id">
                                    <option value="null" {{ ($profile->department_id) == null ? 'selected' : '' }}>N/A</option>
                                    @foreach($departments as $department)
                                    <option value="{{ $department->id }}" {{ ($profile->department_id) == ($department->id) ? 'selected' : '' }}>{{ $department->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row py-3">
                            <div class="form-group col-md-4">
                                <label for="confirm_at">{{ __('Confirm Since') }}</label>
                                <input readonly disabled type="text" class="form-control form-control-sm" name="confirm_at" value="{{ $profile->confirm_at ?? '' }}" placeholder="">
                            </div>
                            <div class="form-group col-md-4">
                                <label for="is_permanent">{{ __('Is Permanent') }}</label>
                                <select disabled readonly class="form-control form-control-sm" name="is_permanent">
                                    <option value="1" {{ ($profile->is_permanent) == 1 ? 'selected' : '' }}>Yes</option>
                                    <option value="0" {{ ($profile->is_permanent) == 0 ? 'selected' : '' }}>No</option>
                                </select>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="created_at">{{ __('Profile Created At') }}</label>
                                <input readonly disabled type="text" class="form-control form-control-sm" name="created_at" value="{{ Auth::user()->created_at }}" placeholder="">
                            </div>
                        </div>
                        <p>{{$message ?? ''}}</p>
                        <button type="submit" class="btn btn-success  btn-sm"><i class="fa fa-upload" style="color: #fff"></i> Update</button>
                    </form>
                </div>

            </div>
        </div>
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><i class="fa fa-clock"></i> {{ __('Change Password') }}</div>
                <div class="card-body">
                    <div class="row">
                        <form action="{{route('changePassword')}}" method="POST">
                            @csrf
                            <label for="content">{{ __('Enter your new password') }}</label>
                            <input type='password' required class="form-control form-control-sm" style="resize: none;" placeholder="" name="newPassword"></input>
                            <br>
                            <label for="content">{{ __('Password Confirmation') }}</label>
                            <input type='password' required class="form-control form-control-sm" style="resize: none;" placeholder="" name="confirmPassword"></input>
                            <p></p>
                            <button type=" submit" class="btn btn-success  btn-sm"><i class="fa fa-upload" style="color: #fff"></i> Change Now</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12 py-4">
            <div class="card">
                <div class="card-header"><i class="fa fa-clock"></i> {{ __('Ticket') }}</div>
                <div class="card-body">
                    <div class="row">
                        <form action="{{route('postTicket')}}" method="POST">
                            @csrf
                            <label for="content">{{ __('Raise Ticket') }}</label>
                            <textarea rows="5" required class="form-control form-control-sm" style="resize: none;" placeholder="" name="content"></textarea>
                            <p></p>
                            <button type=" submit" class="btn btn-success  btn-sm"><i class="fa fa-upload" style="color: #fff"></i> Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><i class="fa fa-download"></i> {{ __('Raised Ticket') }}</div>

                <div class="card-body">

                    <table id="payrollList" class="table table-hover table-bordered">
                        <thead>
                            <tr>
                                <th scope="col" style="font-weight: bold !important; width:10%"># Serial No.</th>
                                <th scope="col" style="font-weight: bold !important;">Content</th>
                                <th scope="col" style="font-weight: bold !important; width:10%">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($tickets as $ticket)
                            <form action="{{route('closeTicket')}}" method="POST">
                                @csrf
                                <tr>
                                    <th scope="row">{{$ticket->id}}<input name='id' hidden value="{{$ticket->id}}"></th>
                                    <td>{{$ticket->content}}</td>
                                    <td><button onclick="return confirm('Are you sure?')" type="submit" class="btn btn-outline-success btn-sm">Close</button></td>
                                </tr>
                            </form>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

<script type="text/javascript">
    $(function() {
        $('#payrollList').DataTable();
    });
</script>
@endsection