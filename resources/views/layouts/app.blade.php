<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.bunny.net/css?family=Nunito" rel="stylesheet">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.13.2/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.0-2/css/fontawesome.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.0-2/css/all.min.css" />

    <!-- Scripts -->
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.css">
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.js"></script>
    @vite(['resources/sass/app.scss', 'resources/css/app.css', 'resources/js/app.js'])
</head>

<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-primary shadow-sm navbar-fixed-top">
            <div class="container-fluid">
                <a class="navbar-brand font-white" href="{{ url('/') }}">
                    {{ config('app.name', 'Laravel') }}
                </a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav me-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ms-auto">
                        <!-- Authentication Links -->
                        @guest
                        @if (Route::has('login'))
                        <li class="nav-item">
                            <a class="nav-link font-white" href="{{ route('login') }}"><i class="fa fa-sign-in"></i> {{ __('Login') }}</a>
                        </li>
                        @endif

                        @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link font-white" href="{{ route('register') }}"><i class="fa fa-sign-out"></i> {{ __('Register') }}</a>
                        </li>
                        @endif
                        @else
                        <li class="nav-item">
                            <a class="nav-link font-white" href="{{ route('applyLeave') }}"><i class="fa fa-edit"></i> {{ __('Apply Leave') }}</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link font-white" href="{{ route('payroll') }}"><i class="fa fa-money-check-alt"></i> {{ __('Payroll') }}</a>
                        </li>
                        @if(Auth::user()->is_admin == true)
                        <li class="nav-item">
                            <a class="nav-link font-white" href="{{ route('approveLeave') }}"><i class="fa fa-check"></i> {{ __('Approve Leave') }}</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link font-white" href="{{ route('employee') }}"><i class="fa fa-users"></i> {{ __('Employee') }}</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link font-white" href="{{ route('approvalDownloadPayroll') }}"><i class="fa fa-download"></i> {{ __('Request Payslip') }}</a>
                        </li>
                        <!-- <li class="nav-item">
                            <a class="nav-link font-white" href="{{ route('jobApplication') }}"><i class="fa fa-list"></i> {{ __('Job Application') }}</a>
                        </li> -->
                        <li class="nav-item">
                            <a class="nav-link font-white" href="{{ route('configureCompany') }}"><i class="fa fa-building"></i> {{ __('Configure Company') }}</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link font-white" href="{{ route('leaveSettings') }}"><i class="fa fa-cog"></i> {{ __('Leave Settings') }}</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link font-white" href="{{ route('uploadPayroll') }}"><i class="fa fa-upload"></i> {{ __('Upload Payroll') }}</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link font-white" href="{{ route('ticket') }}"><i class="fa fa-upload"></i> {{ __('View Ticket') }}</a>
                        </li>
                        @endif
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link font-white dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                <i class="fa fa-user"></i> {{ Auth::user()->user_id }}
                            </a>

                            <div class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                    @csrf
                                </form>
                            </div>
                        </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>
    <footer class="fixed-bottom"><?php echo "&copy; cloud HR system. All Right Reserved. " . date('Y') ?></footer>
</body>

</html>