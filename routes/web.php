<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::post('/home/updateProfile', [App\Http\Controllers\HomeController::class, 'updateProfile'])->name('updateProfile');
Route::post('/home/postTicket', [App\Http\Controllers\HomeController::class, 'postTicket'])->name('postTicket');
Route::post('/home/closeTicket', [App\Http\Controllers\HomeController::class, 'closeTicket'])->name('closeTicket');
Route::post('/home/changePassword', [App\Http\Controllers\HomeController::class, 'changePassword'])->name('changePassword');

Route::get('/applyLeave', [App\Http\Controllers\MenuController::class, 'applyLeave'])->name('applyLeave');
Route::post('/applyLeave/applyLeaveNow', [App\Http\Controllers\LeaveController::class, 'applyLeaveNow'])->name('applyLeaveNow');
Route::post('/applyLeave/withdrawLeave', [App\Http\Controllers\LeaveController::class, 'withdrawLeave'])->name('withdrawLeave');
Route::get('/payroll', [App\Http\Controllers\MenuController::class, 'payroll'])->name('payroll');
Route::get('/payroll/generatePayslip/{id}', [App\Http\Controllers\PDFController::class, 'generatePayslip'])->name('generatePayslip');
Route::get('/payroll/requestPayslip/{id}', [App\Http\Controllers\PDFController::class, 'requestPayslip'])->name('requestPayslip');
Route::get('/payroll/cancelRequestPayslip/{id}', [App\Http\Controllers\PDFController::class, 'cancelRequestPayslip'])->name('cancelRequestPayslip');
Route::get('/approvalDownloadPayrollLeader', [App\Http\Controllers\PDFController::class, 'approvalDownloadPayrollLeader'])->name('approvalDownloadPayrollLeader');
Route::get('/payroll/approveRequestPayslipLeader/{id}', [App\Http\Controllers\PDFController::class, 'approveRequestPayslipLeader'])->name('approveRequestPayslipLeader');
Route::get('/payroll/rejectRequestPayslipLeader/{id}', [App\Http\Controllers\PDFController::class, 'rejectRequestPayslipLeader'])->name('rejectRequestPayslipLeader');

Route::middleware('is_admin')->group(function () {
    Route::get('/payroll/approveRequestPayslip/{id}', [App\Http\Controllers\PDFController::class, 'approveRequestPayslip'])->name('approveRequestPayslip');
    Route::get('/payroll/rejectRequestPayslip/{id}', [App\Http\Controllers\PDFController::class, 'rejectRequestPayslip'])->name('rejectRequestPayslip');
    Route::get('/ticket', [App\Http\Controllers\MenuController::class, 'ticket'])->name('ticket');
    Route::post('/ticket/closeTicket', [App\Http\Controllers\MenuController::class, 'closeTicket'])->name('closeTicket2');
    Route::get('/approveLeave', [App\Http\Controllers\MenuController::class, 'approveLeave'])->name('approveLeave');
    Route::get('/approvalDownloadPayroll', [App\Http\Controllers\PDFController::class, 'approvalDownloadPayroll'])->name('approvalDownloadPayroll');
    Route::post('/applyLeave/approveUserLeave', [App\Http\Controllers\LeaveController::class, 'approveUserLeave'])->name('approveUserLeave');
    Route::get('/employee', [App\Http\Controllers\MenuController::class, 'employee'])->name('employee');
    Route::get('/employee/contractPDF/{id}', [App\Http\Controllers\PDFController::class, 'ContractPDF'])->name('contractPDF');
    Route::get('/employee/confirmationPDF/{id}', [App\Http\Controllers\PDFController::class, 'ConfirmationPDF'])->name('confirmationPDF');
    Route::get('/employee/viewprofile/{id}', [App\Http\Controllers\EmployeeController::class, 'viewprofile'])->name('viewprofile');
    Route::post('/employee/updateUserProfile', [App\Http\Controllers\EmployeeController::class, 'updateUserProfile'])->name('updateUserProfile');
    Route::get('/employee/disableProfile/{id}', [App\Http\Controllers\EmployeeController::class, 'disableProfile'])->name('disableProfile');
    Route::get('/employee/setAsAdmin/{id}', [App\Http\Controllers\EmployeeController::class, 'setAsAdmin'])->name('setAsAdmin');
    Route::get('/employee/resetPassword/{id}', [App\Http\Controllers\EmployeeController::class, 'resetPassword'])->name('resetPassword');
    Route::get('/jobApplication', [App\Http\Controllers\MenuController::class, 'jobApplication'])->name('jobApplication');
    Route::get('/configureCompany', [App\Http\Controllers\MenuController::class, 'configureCompany'])->name('configureCompany');
    Route::get('/leaveSettings', [App\Http\Controllers\MenuController::class, 'leaveSettings'])->name('leaveSettings');
    Route::get('/uploadPayroll', [App\Http\Controllers\MenuController::class, 'uploadPayroll'])->name('uploadPayroll');
    Route::post('/uploadPayroll/uploadcsv', [App\Http\Controllers\PayrollController::class, 'uploadcsv'])->name('uploadcsv');
    Route::get('/uploadPayroll/deletePayroll/{id}', [App\Http\Controllers\PayrollController::class, 'deletePayroll'])->name('deletePayroll');
    Route::post('/configureCompany/createCompany', [App\Http\Controllers\ConfigureCompanyController::class, 'createCompany'])->name('createCompany');
    Route::post('/configureCompany/createDepartment', [App\Http\Controllers\ConfigureCompanyController::class, 'createDepartment'])->name('createDepartment');
    Route::post('/configureCompany/createPosition', [App\Http\Controllers\ConfigureCompanyController::class, 'createPosition'])->name('createPosition');
    Route::post('/configureCompany/deleteCompany', [App\Http\Controllers\ConfigureCompanyController::class, 'deleteCompany'])->name('deleteCompany');
    Route::post('/leaveSettings/addLeaveType', [App\Http\Controllers\LeaveSettingsController::class, 'addLeaveType'])->name('addLeaveType');
    Route::get('/leaveSettings/changeProrateStatus/{id}', [App\Http\Controllers\LeaveSettingsController::class, 'changeProrateStatus'])->name('changeProrateStatus');
    Route::post('/leaveSettings/deleteLeave', [App\Http\Controllers\LeaveSettingsController::class, 'deleteLeave'])->name('deleteLeave');
    Route::post('/leaveSettings/deleteCompanyLeave', [App\Http\Controllers\LeaveSettingsController::class, 'deleteCompanyLeave'])->name('deleteCompanyLeave');
    Route::post('/leaveSettings/updateCompanyLeave', [App\Http\Controllers\LeaveSettingsController::class, 'updateCompanyLeave'])->name('updateCompanyLeave');
});
Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
