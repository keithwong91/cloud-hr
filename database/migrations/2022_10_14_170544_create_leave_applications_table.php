<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leave_applications', function (Blueprint $table) {
            $table->id();
            $table->string('user_id');
            $table->string('leave_id');
            $table->date('from');
            $table->date('to');
            $table->string('day');
            $table->string('daytime');
            $table->string('reason')->nullable();
            $table->string('attachment_name')->nullable();
            $table->string('attachment')->nullable();
            $table->string('remark')->nullable();
            $table->integer('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leave_applications');
    }
};
