<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payrolls', function (Blueprint $table) {
            $table->id();
            $table->string('user_id');
            $table->string('empno');
            $table->string('empnirc');
            $table->string('empname');
            $table->decimal('basic')->default('0');
            $table->decimal('npl')->default('0');
            $table->decimal('addpay')->default('0');
            $table->decimal('ot')->default('0');
            $table->decimal('bonus')->default('0');
            $table->decimal('meal')->default('0');
            $table->decimal('claims')->default('0');
            $table->decimal('inc1')->default('0');
            $table->decimal('other2')->default('0');
            $table->decimal('other3')->default('0');
            $table->decimal('other4')->default('0');
            $table->decimal('other5')->default('0');
            $table->decimal('grosspay')->default('0');
            $table->decimal('empepf')->default('0');
            $table->decimal('empsocso')->default('0');
            $table->decimal('empeis')->default('0');
            $table->decimal('emppcb')->default('0');
            $table->decimal('nettpay')->default('0');
            $table->decimal('repf')->default('0');
            $table->decimal('rsocso')->default('0');
            $table->decimal('reis')->default('0');
            $table->string('month');
            $table->string('year');
            $table->boolean('active')->default(true);
            $table->timestamps();
        });
    }

    // ALTER TABLE `cloud_hr`.`payrolls` ADD COLUMN `hostel` DECIMAL(8,2) NOT NULL DEFAULT '0.00' AFTER `updated_at`;
    // ALTER TABLE `cloud_hr`.`payrolls` ADD COLUMN `dispute` DECIMAL(8,2) NOT NULL DEFAULT '0.00' AFTER `hostel`;
    // ALTER TABLE `cloud_hr`.`payrolls` ADD COLUMN `company` VARCHAR(45) NOT NULL DEFAULT 'Asia Fibre Solutions Sdn Bhd (128608-M)' AFTER `dispute`;
    // ALTER TABLE `cloud_hr`.`payrolls` ADD COLUMN `request` INT NOT NULL DEFAULT 0 AFTER `company`;
    // ALTER TABLE `cloud_hr`.`payrolls` ADD COLUMN `plencash` DECIMAL(8,2) NOT NULL DEFAULT '0.00' AFTER `company`;
    // ALTER TABLE `cloud_hr`.`payrolls` ADD COLUMN `projallow` DECIMAL(8,2) NOT NULL DEFAULT '0.00' AFTER `company`;
    // ALTER TABLE `cloud_hr`.`payrolls` ADD COLUMN `cnyspcom` DECIMAL(8,2) NOT NULL DEFAULT '0.00' AFTER `company`;
    // ALTER TABLE `cloud_hr`.`payrolls` ADD COLUMN `referral` DECIMAL(8,2) NOT NULL DEFAULT '0.00' AFTER `hostel`;
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payrolls');
    }
};
