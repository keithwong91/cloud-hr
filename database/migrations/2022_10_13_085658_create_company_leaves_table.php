<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_leaves', function (Blueprint $table) {
            $table->id();
            $table->string('company_id');
            $table->string('leave_id');
            $table->integer('day')->default(1);
            $table->boolean('active')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_leaves');
    }

    // ALTER TABLE `cloud_hr`.`companies` ADD COLUMN `ranking` INT NULL DEFAULT 99 AFTER `type`;

};
