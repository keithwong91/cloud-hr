<?php

namespace App\Http\Controllers;

use App\Models\Company;
use App\Models\CompanyLeave;
use App\Models\Leave;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class ConfigureCompanyController extends Controller
{
   public function createCompany(Request $request)
    {
        $createCompanyMessage = null;
        $existing = Company::where(['name' => $request->all()['name'], 'active' => true, 'type' => 'company'])->count();
        if($existing > 0){
            $createCompanyMessage = 'Duplicate company name';
        }else{
            $company = new Company();
            $company->name = $request->all()['name'];
            $company->type = 'company';
            $company->save();
            $createCompanyMessage = 'Company created';
        }
        $company_id = Company::where(['active' => true, 'name' => $request->all()['name'], 'type' => 'company'])->first()['id'];
        $leaves = Leave::where(['active' => true, 'apply_for_all_companies' => true])->get();
        foreach ($leaves as $leave) {
            $companyLeave = new CompanyLeave();
            $companyLeave->company_id = $company_id;
            $companyLeave->leave_id = $leave->id;
            $companyLeave->day = $leave->day;
            $companyLeave->save();
        }

        $companies = Company::where(['active' => true, 'type' => 'company'])->get();
        $departments = Company::where(['active' => true, 'type' => 'department'])->get();
        $positions = Company::where(['active' => true, 'type' => 'position'])->get();
        return view('configureCompany', ['companies' => $companies, 'departments' => $departments, 'positions' => $positions, 'createCompanyMessage' => $createCompanyMessage]);
    }

    public function createDepartment(Request $request)
    {
        $createDepartmentMessage = null;
        $existing = Company::where(['name' => $request->all()['name'], 'active' => true, 'type' => 'department'])->count();
        if ($existing > 0) {
            $createDepartmentMessage = 'Duplicate department name';
        } else {
            $company = new Company();
            $company->name = $request->all()['name'];
            $company->type = 'department';
            $company->save();
            $createDepartmentMessage = 'Department created';
        }
        $companies = Company::where(['active' => true, 'type' => 'company'])->get();
        $departments = Company::where(['active' => true, 'type' => 'department'])->get();
        $positions = Company::where(['active' => true, 'type' => 'position'])->get();
        return view('configureCompany', ['companies' => $companies, 'departments' => $departments, 'positions' => $positions, 'createDepartmentMessage' => $createDepartmentMessage]);
    }

    public function createPosition(Request $request)
    {
        $createPositionMessage = null;
        $existing = Company::where(['name' => $request->all()['name'], 'active' => true, 'type' => 'position'])->count();
        if ($existing > 0) {
            $createPositionMessage = 'Duplicate position name';
        } else {
            $company = new Company();
            $company->name = $request->all()['name'];
            $company->ranking = $request->all()['ranking'];
            $company->type = 'position';
            $company->save();
            $createPositionMessage = 'Position created';
        }
        $companies = Company::where(['active' => true, 'type' => 'company'])->get();
        $departments = Company::where(['active' => true, 'type' => 'department'])->get();
        $positions = Company::where(['active' => true, 'type' => 'position'])->get();
        return view('configureCompany', ['companies' => $companies, 'departments' => $departments, 'positions' => $positions, 'createPositionMessage' => $createPositionMessage]);
    }

    public function deleteCompany(Request $request)
    {
        if(isset($request->all()['update'])) {
            Company::where('id', $request->all()['id'])->update(['ranking' => $request->all()['ranking']]);
        } else {
            Company::where('id', $request->all()['id'])->update(['active' => false]);
        }
        $companies = Company::where(['active' => true, 'type' => 'company'])->get();
        $departments = Company::where(['active' => true, 'type' => 'department'])->get();
        $positions = Company::where(['active' => true, 'type' => 'position'])->get();
        return view('configureCompany', ['companies' => $companies, 'departments' => $departments, 'positions' => $positions]);
    }
}
