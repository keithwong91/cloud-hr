<?php

namespace App\Http\Controllers;

use App\Models\LeaveApplication;
use App\Models\Profile;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class LeaveController extends Controller
{
    public function applyLeaveNow(Request $request){
        $leaves = $this->getLeave();
        foreach($leaves as $leave) {
            if($leave->id == $request->all()['leave_id']){
                $taken = DB::select('select sum(day) as taken from leave_applications where status in (0,1) and leave_id = ? and user_id =?', [$request->all()['leave_id'], Auth::user()->id])[0]->taken;
                if(($taken + $request->all()['day']) > $leave->day){
                    $la = DB::select('SELECT la.*, l.leave_name FROM leave_applications la inner join leaves l on l.id = la.leave_id where la.user_id = ? order by la.id desc', [Auth::user()->id]);
                    return view('applyLeave', ['leaves' => $this->getLeave(), 'leaveApplication' => $la, 'message' => 'Your leave is not enough to apply on this application']);
                }
            }
        }

        $checkFrom = LeaveApplication::whereIn('status', array(0, 1))->where('user_id', Auth::user()->id)->whereBetween('from', [$request->all()['from'], $request->all()['to']])->count(); 
        $checkTo = LeaveApplication::whereIn('status', array(0, 1))->where('user_id', Auth::user()->id)->whereBetween('to', [$request->all()['from'], $request->all()['to']])->count(); 
        if($checkFrom > 0 || $checkTo > 0){
            $la = DB::select('SELECT la.*, l.leave_name FROM leave_applications la inner join leaves l on l.id = la.leave_id where la.user_id = ? order by la.id desc', [Auth::user()->id]);
            return view('applyLeave', ['leaves' => $this->getLeave(), 'leaveApplication' => $la, 'message' => 'You have pending or successful leave on the date range']);
        }
        $leaveApplication = new LeaveApplication();
        $leaveApplication->user_id = Auth::user()->id;
        $leaveApplication->leave_id = $request->all()['leave_id'];
        $leaveApplication->from = $request->all()['from'];
        $leaveApplication->to = $request->all()['to'];
        $leaveApplication->day = $request->all()['day'];
        $leaveApplication->daytime = $request->all()['daytime'];
        $leaveApplication->reason = $request->all()['reason'];
        $file = $request->file('attachment');
        if($file){
            $filename = date('YmdHi') . $file->getClientOriginalName();
            $file->move(public_path('attachment'), $filename);
            $leaveApplication->attachment_name = $file->getClientOriginalName();
            $leaveApplication->attachment = $filename;
        }
        $leaveApplication->save();
        $la = DB::select('SELECT la.*, l.leave_name FROM leave_applications la inner join leaves l on l.id = la.leave_id where la.user_id = ? order by la.id desc', [Auth::user()->id]);

        return view('applyLeave', ['leaves' => $this->getLeave(), 'leaveApplication' => $la, 'message' => 'Leave Apply Successful']);
    }

    public function getLeave()
    {
        $is_permanent = false;
        $is_permanent = Profile::where(['active' => true, 'user_id' => Auth::user()->id])->first() ? Profile::where(['active' => true, 'user_id' => Auth::user()->id])->first()->is_permanent : false;
        $created_at = User::where(['active' => true, 'id' => Auth::user()->id])->first()->created_at;
        $confirm_at = Profile::where(['active' => true, 'id' => Auth::user()->id])->first()->confirm_at;
        $company = DB::select('SELECT company_id, is_permanent, created_at, confirm_at FROM profiles where user_id = ' . Auth::user()->id) ? DB::select('SELECT company_id, is_permanent, created_at, confirm_at FROM profiles where user_id = ' . Auth::user()->id)[0] : [];
        if ($company != []) {
            if ($company->company_id == null) {
                $company->company_id = 0;
            }
            if ($is_permanent) {
                $company_leaves = DB::select('select cl.*, l.leave_name, l.is_for_permanent_only, l.no_prorate from company_leaves cl inner join leaves l  on l.id = cl.leave_id where company_id = ' . $company->company_id . ' and l.active = true and cl.active = true');
            } else {
                $company_leaves = DB::select('select cl.*, l.leave_name, l.is_for_permanent_only, l.no_prorate from company_leaves cl inner join leaves l  on l.id = cl.leave_id where company_id = ' . $company->company_id . ' and l.active = true and cl.active = true and l.is_for_permanent_only = 0');
            }

            foreach ($company_leaves as $company_leave) {
                $company_leave->day = $company_leave->no_prorate ? $company_leave->day : $this->getProrate($created_at, $company_leave->day, $confirm_at, $company_leave->is_for_permanent_only);
                $company_leave->taken = DB::select('select sum(day) as taken from leave_applications where status in (0,1) and leave_id = ? and user_id =?', [$company_leave->leave_id, Auth::user()->id])[0]->taken;
            }
            return $company_leaves;
        } else {
            return [];
        }
    }

    public function getProrate($created_at, $leave_day, $confirm_at, $is_permanent)
    {
        $today = Carbon::now();
        $confirm_at = Carbon::parse($confirm_at);
        $created_at = Carbon::parse($created_at);
        $confirm_at_month_diff = $today->diffInMonths($confirm_at);
        $created_at_month_diff = $today->diffInMonths($created_at);
        $prorateMonth = 0;

        if ($created_at_month_diff > 12) {
            $prorateMonth = $today->month - $created_at->month;
        } else {
            $prorateMonth = $created_at_month_diff;
        }
        if ($confirm_at_month_diff > 12) {
            $confirm_at_month_diff = 0;
        }
        $appicable_day = ($leave_day / 12) * $prorateMonth;
        if($is_permanent && $confirm_at_month_diff > 0){
            $appicable_day = round($appicable_day, 0) - (12 -$confirm_at_month_diff);
        }else{
            $appicable_day = round($appicable_day, 0);
        }

        return $appicable_day;
    }

    public function withdrawLeave(Request $request)
    {
        LeaveApplication::where(['status' => 0, 'id'=>$request->all()['leave_id']])->update(['status' => 3]);
        $leaveApplication = DB::select('SELECT la.*, l.leave_name FROM leave_applications la inner join leaves l on l.id = la.leave_id where la.user_id = ? order by la.id desc', [Auth::user()->id]);
        return view('applyLeave', ['leaves' => $this->getLeave(), 'leaveApplication' => $leaveApplication]);
    }

    public function approveUserLeave(Request $request)
    {
        $requestStirng = json_encode($request->all());
        if (str_contains($requestStirng, 'approve')) {
            $status = 1;
        }
        if (str_contains($requestStirng, 'decline')) {
            $status = 2;
        }

        LeaveApplication::where(['status' => 0, 'id' => $request->all()['leave_id']])->update(['status' => $status, 'remark' => $request->all()['remark']]);
        $leaveApplication = DB::select('SELECT la.*, l.leave_name, p.name FROM leave_applications la inner join leaves l on l.id = la.leave_id inner join profiles p on p.user_id = la.user_id where la.status in (0) order by la.id desc');
        $approvedleave = DB::select('SELECT la.*, l.leave_name, p.name FROM leave_applications la inner join leaves l on l.id = la.leave_id inner join profiles p on p.user_id = la.user_id where la.status in (1) order by la.id desc');
        return view('approveLeave', ['leaves' => $this->getLeave(), 'leaveApplication' => $leaveApplication, 'approvedleave' => $approvedleave]);
        
    }
}
