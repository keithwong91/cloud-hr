<?php

namespace App\Http\Controllers;

use App\Models\Company;
use App\Models\Leave;
use App\Models\Profile;
use App\Models\Ticket;
use App\Models\User;
use App\Models\Payroll;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class MenuController extends Controller
{
    public function applyLeave()
    {
        $leave = [];
        $profile = Profile::where(['active' => true, 'user_id' => Auth::user()->id])->first();
        if ($profile->company_id != null) {
            $leave = $this->getLeave();
        }
        $leaveApplication = DB::select('SELECT la.*, l.leave_name FROM leave_applications la inner join leaves l on l.id = la.leave_id where la.user_id = ? order by la.id desc',[Auth::user()->id]);
        return view('applyLeave', ['leaves' => $leave, 'leaveApplication' => $leaveApplication]);
    }

    public function payroll()
    {
        $showPayslipApproval = Profile::where(['active' => true, 'user_id' => Auth::user()->id])->first()->grade_id == 10;
        $data = Payroll::where(['active' => true, 'user_id' => Auth::user()->user_id])->orderBy('created_at', 'DESC')->get();
        return view('payroll', ['payroll' => $data, 'showPayslipApproval' => $showPayslipApproval]);
    }

    public function approveLeave()
    {
        $leaveApplication = DB::select('SELECT la.*, l.leave_name, p.name FROM leave_applications la inner join leaves l on l.id = la.leave_id inner join profiles p on p.user_id = la.user_id where la.status in (0) order by la.id desc');
        $approvedleave = DB::select('SELECT la.*, l.leave_name, p.name FROM leave_applications la inner join leaves l on l.id = la.leave_id inner join profiles p on p.user_id = la.user_id where la.status in (1) order by la.id desc');
        return view('approveLeave', ['leaves' => $this->getLeave(), 'leaveApplication' => $leaveApplication, 'approvedleave'=> $approvedleave]);
    }

    public function employee()
    {
        $profiles = DB::select('SELECT p.* , u.user_id as nric, u.is_admin FROM profiles p inner join users u on u.id = p.user_id where u.active = true');
        return view('employee', ['profiles' => $profiles]);
    }

    public function jobApplication()
    {
        return view('jobApplication');
    }

    public function configureCompany()
    {
        $companies = Company::where(['active'=> true, 'type' => 'company'])->get();
        $departments = Company::where(['active'=> true, 'type' => 'department'])->get();
        $positions = Company::where(['active' => true, 'type' => 'position'])->get();
        return view('configureCompany', ['companies' => $companies, 'departments' => $departments, 'positions' => $positions]);
    }

    public function leaveSettings()
    {
        $companies = Company::where(['active' => true, 'type' => 'company'])->get();
        $leaves = Leave::where(['active' => true])->get();
        $companyLeaves = DB::select('SELECT cl.id, c.name, l.leave_name, cl.day, cl.created_at, cl.updated_at, l.is_for_permanent_only FROM company_leaves cl 
        inner join companies c on c.id = cl.company_id
        inner join leaves l on l.id = cl.leave_id
        where c.active = true and l.active = true and cl.active = true order by cl.company_id');
        return view('leaveSettings', ['companies' => $companies, 'leaves' => $leaves, 'companyLeaves' => $companyLeaves]);
    }

    public function uploadPayroll()
    {
        $data = Payroll::where(['active' => true])->orderBy('created_at', 'DESC')->get();
        return view('uploadPayroll', ['payroll' => $data]);
    }

    public function ticket()
    {
        $ticket = DB::select('select t.*, p.name from tickets t left join profiles p on p.user_id = t.user_id where t.status = 0;');
        return view('ticket', ['tickets' => $ticket]);
    }

    public function getLeave()
    {
        $is_permanent = false;
        $is_permanent = Profile::where(['active' => true, 'user_id' => Auth::user()->id])->first() ? Profile::where(['active' => true, 'user_id' => Auth::user()->id])->first()->is_permanent : false;
        $created_at = User::where(['active' => true, 'id' => Auth::user()->id])->first()->created_at;
        $confirm_at = Profile::where(['active' => true, 'id' => Auth::user()->id])->first()->confirm_at;
        $company = DB::select('SELECT company_id, is_permanent, created_at, confirm_at FROM profiles where user_id = ' . Auth::user()->id) ? DB::select('SELECT company_id, is_permanent, created_at, confirm_at FROM profiles where user_id = ' . Auth::user()->id)[0] : [];
        if($company != []) {
            if ($company->company_id == null) {
                $company->company_id = 0;
            }
            if ($is_permanent) {
                $company_leaves = DB::select('select cl.*, l.leave_name, l.is_for_permanent_only, l.no_prorate from company_leaves cl inner join leaves l  on l.id = cl.leave_id where company_id = ' . $company->company_id . ' and l.active = true and cl.active = true');
            } else {
                $company_leaves = DB::select('select cl.*, l.leave_name, l.is_for_permanent_only, l.no_prorate from company_leaves cl inner join leaves l  on l.id = cl.leave_id where company_id = ' . $company->company_id . ' and l.active = true and cl.active = true and l.is_for_permanent_only = 0');
            }  
            foreach ($company_leaves as $company_leave) {
                $company_leave->day = $company_leave->no_prorate ? $company_leave->day : $this->getProrate($created_at, $company_leave->day, $confirm_at, $company_leave->is_for_permanent_only);
                $company_leave->taken = DB::select('select sum(day) as taken from leave_applications where status in (0,1) and leave_id = ? and user_id =?', [$company_leave->leave_id, Auth::user()->id])[0]->taken;
            }
            return $company_leaves;
        }else{
            return[];
        }
    }

    public function getProrate($created_at, $leave_day, $confirm_at, $is_permanent)
    {
        $today = Carbon::now();
        $confirm_at = Carbon::parse($confirm_at);
        $created_at = Carbon::parse($created_at);
        $confirm_at_month_diff = $today->diffInMonths($confirm_at);
        $created_at_month_diff = $today->diffInMonths($created_at);
        $prorateMonth = 0;

        if ($created_at_month_diff > 12) {
            $prorateMonth = $today->month - $created_at->month;
        } else {
            $prorateMonth = $created_at_month_diff;
        }
        if ($confirm_at_month_diff > 12) {
            $confirm_at_month_diff = 0;
        }
        $appicable_day = ($leave_day / 12) * $prorateMonth;
        if($is_permanent && $confirm_at_month_diff > 0){
            $appicable_day = round($appicable_day, 0) - (12 -$confirm_at_month_diff);
        }else{
            $appicable_day = round($appicable_day, 0);
        }

        return $appicable_day;
    }

    public function closeTicket(Request $request)
    {
        Ticket::where(['id' => $request->all()['id']])->update(['status' => 2]);
        return $this->ticket();
    }
}
