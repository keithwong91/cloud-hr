<?php

namespace App\Http\Controllers;

use App\Models\Company;
use App\Models\Profile;
use App\Models\Ticket;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $leave = [];
        $profile = Profile::where(['active' => true, 'user_id' => Auth::user()->id])->first();
        if($profile == null){
            $new = new Profile();
            $new->user_id = Auth::user()->id;
            $new->save();
        }
        $profile = Profile::where(['active' => true, 'user_id' => Auth::user()->id])->first();
        if($profile->company_id != null){
            $leave = $this->getLeave();
        }
        $tickets = Ticket::where(['status' => 0,'user_id' => Auth::user()->id])->get();
        $companies = Company::where(['active' => true, 'type' => 'company'])->get();
        $departments = Company::where(['active' => true, 'type' => 'department'])->get();
        $positions = Company::where(['active' => true, 'type' => 'position'])->get();
        return view('home', ['profile' => $profile, 'companies' => $companies, 'departments' => $departments, 'positions' => $positions, 'leaves' => $leave, 'tickets' => $tickets]);
    }

    public function updateProfile(Request $request)
    {
        $request->offsetUnset('_token');
        Profile::updateOrCreate(
            ['user_id' => Auth::user()->id],
            $request->all()
        );
        $message = 'Profile updated';
        $companies = Company::where(['active' => true, 'type' => 'company'])->get();
        $departments = Company::where(['active' => true, 'type' => 'department'])->get();
        $positions = Company::where(['active' => true, 'type' => 'position'])->get();
        $profile = Profile::where(['active' => true, 'user_id' => Auth::user()->id])->first();

        return $this->index();
        // return view('home', ['profile' => $profile, 'companies' => $companies, 'departments' => $departments, 'positions' => $positions, 'message' => $message,'leaves' => $this->getleave()]);
    }

    public function getLeave(){
        $is_permanent = false;
        $is_permanent = Profile::where(['active' => true, 'user_id' => Auth::user()->id])->first() ? Profile::where(['active' => true, 'user_id' => Auth::user()->id])->first()->is_permanent : false;
        $created_at = User::where(['active' => true, 'id' => Auth::user()->id])->first()->created_at;
        $confirm_at = Profile::where(['active' => true, 'id' => Auth::user()->id])->first()->confirm_at;
        $company = DB::select('SELECT company_id, is_permanent, created_at, confirm_at FROM profiles where user_id = ' . Auth::user()->id) ? DB::select('SELECT company_id, is_permanent, created_at, confirm_at FROM profiles where user_id = ' . Auth::user()->id)[0] : [];

        if ($company != [] ) {
            if($company->company_id == null) {
                $company->company_id = 0;
            }
            if ($is_permanent) {
                $company_leaves = DB::select('select cl.*, l.leave_name, l.is_for_permanent_only, l.no_prorate from company_leaves cl inner join leaves l  on l.id = cl.leave_id where company_id = ' . $company->company_id . ' and l.active = true and cl.active = true');
            } else {
                $company_leaves = DB::select('select cl.*, l.leave_name, l.is_for_permanent_only, l.no_prorate from company_leaves cl inner join leaves l  on l.id = cl.leave_id where company_id = ' . $company->company_id . ' and l.active = true and cl.active = true and l.is_for_permanent_only = 0');
            }

            foreach ($company_leaves as $company_leave) {
                $company_leave->day = $company_leave->no_prorate ? $company_leave->day : $this->getProrate($created_at, $company_leave->day, $confirm_at, $company_leave->is_for_permanent_only);
                $company_leave->taken = DB::select('select sum(day) as taken from leave_applications where status in (0,1) and leave_id = ? and user_id =?', [$company_leave->leave_id, Auth::user()->id])[0]->taken;
            }
            return $company_leaves;
        } else {
            return [];
        }
    }

    public function getProrate($created_at, $leave_day, $confirm_at, $is_permanent){
        $today = Carbon::now();
        $confirm_at = Carbon::parse($confirm_at);
        $created_at = Carbon::parse($created_at);
        $confirm_at_month_diff = $today->diffInMonths($confirm_at);
        $created_at_month_diff = $today->diffInMonths($created_at);
        $prorateMonth = 0;

        if ($confirm_at_month_diff > 12) {
            $confirm_at_month_diff = 0;
        }

        if ($created_at_month_diff > 12) {
            $prorateMonth = $today->month - $created_at->month;
        } else {
            $prorateMonth = $created_at_month_diff;
        }
        $appicable_day = ($leave_day / 12) * $prorateMonth;
        if($is_permanent && $confirm_at_month_diff > 0){
            $appicable_day = round($appicable_day, 0) - (12 -$confirm_at_month_diff);
        }else{
            $appicable_day = round($appicable_day, 0);
        }
        
        return $appicable_day;
    }

    public function postTicket(Request $request){
        $ticket = new Ticket();
        $ticket->user_id = Auth::user()->id;
        $ticket->content = $request->all()['content'];
        $ticket->save();
        return $this->index();
    }

    public function closeTicket(Request $request)
    {
        Ticket::where(['id'=> $request->all()['id']])->update(['status'=> 2]);
        return $this->index();
    }

    public function changePassword(Request $request)
    {
        if($request->all()['newPassword'] != $request->all()['confirmPassword']){
            return $this->index();
        }
        User::where(['id'=> Auth::user()->id])->update(['password'=> Hash::make($request->all()['newPassword'])]);
        Auth::logout();
        return redirect('/login');
    }
}
