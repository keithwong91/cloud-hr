<?php

namespace App\Http\Controllers;

use App\Models\Payroll;
use Illuminate\Http\Request;

class PayrollController extends Controller
{
    public function uploadcsv(Request $request)
    {
        $file = $request->file('excel');
        if (!file_exists($file) || !is_readable($file))
        return false;

        $header = null;
        $data = array();
        if (($handle = fopen($file, 'r')) !== false)
        {
            while (($row = fgetcsv($handle, 1000, ',')) !== false)
            {
                if (!$header)
                    $header = $row;
                else
                    $data[] = array_combine($header, $row);
            }
            fclose($handle);
        }
        
        foreach ($data as $payroll) {
            $savePayroll = new Payroll();
            $savePayroll->user_id = str_replace('-', '', $payroll['empnirc']);
            $savePayroll->empno = 0;
            $savePayroll->empnirc = $payroll['empnirc'];
            $savePayroll->empno = $payroll['serialno'];
            $savePayroll->empname = $payroll['empname'];
            $savePayroll->basic = $payroll['basic'];
            $savePayroll->npl = $payroll['npl'];
            $savePayroll->addpay = $payroll['addpay'];
            $savePayroll->ot = $payroll['ot'];
            $savePayroll->bonus = $payroll['bonus'];
            $savePayroll->meal = $payroll['meal'];
            $savePayroll->claims = $payroll['claims'];
            $savePayroll->inc1 = $payroll['inc1'];
            $savePayroll->other2 = $payroll['inc2'];
            $savePayroll->other3 = $payroll['attnallow'];
            $savePayroll->other4 = $payroll['backpay'];
            $savePayroll->other5 = $payroll['other5'];
            $savePayroll->grosspay = $payroll['grosspay'];
            $savePayroll->empepf = $payroll['empepf'];
            $savePayroll->empsocso = $payroll['empsocso'];
            $savePayroll->empeis = $payroll['empeis'];
            $savePayroll->emppcb = $payroll['emppcb'];
            $savePayroll->nettpay = $payroll['nettpay'];
            $savePayroll->repf = $payroll['repf'];
            $savePayroll->rsocso = $payroll['rsocso'];
            $savePayroll->reis = $payroll['reis'];
            $savePayroll->month = $payroll['month'];
            $savePayroll->year = $payroll['year'];
            $savePayroll->hostel = $payroll['hostel'];
            $savePayroll->dispute = $payroll['dispute'];
            $savePayroll->plencash = $payroll['plencash'];
            $savePayroll->projallow = $payroll['projallow'];
            $savePayroll->cnyspcom = $payroll['cnyspcom'];
            $savePayroll->referral = $payroll['referral'];
            if (isset($payroll['company'])){
                $savePayroll->company = $payroll['company'];
            }
            $savePayroll->save();
        }
        $data = Payroll::where(['active' => true])->orderBy('created_at', 'DESC')->get();
        return view('uploadPayroll', ['payroll' => $data]);
    }

    public function deletePayroll(Request $request){
        $payroll_id = $request->route()->parameters['id'];
        Payroll::where('id', $payroll_id)->update(['active' => false]);
        $data = Payroll::where(['active' => true])->orderBy('created_at', 'DESC')->get();
        return view('uploadPayroll', ['payroll' => $data]);
    }
}
