<?php

namespace App\Http\Controllers;

use App\Models\Company;
use App\Models\CompanyLeave;
use App\Models\Leave;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Mockery\Undefined;

class LeaveSettingsController extends Controller
{
    public function addLeaveType(Request $request)
    {
        $addLeaveTypeMsg = null;
        $existing = Leave::where(['leave_name' => $request->all()['leaveName'], 'active' => true])->count();
        $is_for_permanent_only = false;
        $apply_for_all_companies = false;
        $no_prorate = false;
        if(isset($request->all()['permenantOnly'])){
            $is_for_permanent_only = true;
        }
        if(isset($request->all()['no_prorate'])){
            $no_prorate = true;
        }
        if (isset($request->all()['applyForAll'])) {
            $apply_for_all_companies = true;
            $companies = Company::where(['active' => true, 'type' => 'company'])->get();
        }
        if ($existing > 0) {
            $addLeaveTypeMsg = 'Duplicate leave type';
        } else {
            $leave = new Leave();
            $leave->leave_name = $request->all()['leaveName'];
            $leave->day = $request->all()['totalDay'];
            $leave->is_for_permanent_only = $is_for_permanent_only;
            $leave->apply_for_all_companies = $apply_for_all_companies;
            $leave->no_prorate = $no_prorate;
            $leave->save();
            $addLeaveTypeMsg = 'Leave created';
        }
        $companies = Company::where(['active' => true, 'type' => 'company'])->get();
        $leaves = Leave::where(['active' => true])->get();
        $leave_id = Leave::where(['active' => true, 'leave_name' => $request->all()['leaveName']])->first()['id'];
        $leave_day = Leave::where(['active' => true, 'id' => $leave_id])->first()['day'];
        if($apply_for_all_companies){
            foreach ($companies as $company) {
                $companyLeave = new CompanyLeave();
                $companyLeave->company_id = $company->id;
                $companyLeave->leave_id = $leave_id;
                $companyLeave->day = $leave_day;
                $companyLeave->save();
            }
        }
        $leaves = Leave::where(['active' => true])->get();
        $companyLeaves = DB::select('SELECT cl.id, c.name, l.leave_name, cl.day, cl.created_at, cl.updated_at, l.is_for_permanent_only FROM company_leaves cl 
        inner join companies c on c.id = cl.company_id
        inner join leaves l on l.id = cl.leave_id
        where c.active = true and l.active = true and cl.active = true order by cl.company_id');
        return view('leaveSettings', ['companies' => $companies, 'leaves' => $leaves, 'companyLeaves' => $companyLeaves, 'addLeaveTypeMsg' => $addLeaveTypeMsg]);
    }

    public function updateCompanyLeave(Request $request)
    {
        // dd($request->all());
        $existing = CompanyLeave::where(['leave_id'=> $request->all()['leaveType'], 'company_id' => $request->all()['company'], 'active' => true])->first();
        if($existing) {
            CompanyLeave::where(['leave_id' => $request->all()['leaveType'], 'company_id' => $request->all()['company'], 'active' => true])->update(['day' => $request->all()['day']]);
        } else {
            $companyLeave = new CompanyLeave();
            $companyLeave->leave_id = $request->all()['leaveType'];
            $companyLeave->company_id = $request->all()['company'];
            $companyLeave->day = $request->all()['day'];
            $companyLeave->save();
        }
        $companies = Company::where(['active' => true, 'type' => 'company'])->get();
        $leaves = Leave::where(['active' => true])->get();
        $companyLeaves = DB::select('SELECT cl.id, c.name, l.leave_name, cl.day, cl.created_at, cl.updated_at, l.is_for_permanent_only FROM company_leaves cl 
        inner join companies c on c.id = cl.company_id
        inner join leaves l on l.id = cl.leave_id
        where c.active = true and l.active = true and cl.active = true order by cl.company_id');
        return view('leaveSettings', ['companies' => $companies, 'leaves' => $leaves, 'companyLeaves' => $companyLeaves]);
    }

    public function deleteLeave(Request $request)
    {
        Leave::where('id', $request->all()['id'])->update(['active' => false]);
        CompanyLeave::where('leave_id', $request->all()['id'])->update(['active' => false]);
        $companies = Company::where(['active' => true, 'type' => 'company'])->get();
        $leaves = Leave::where(['active' => true])->get();
        $companyLeaves = DB::select('SELECT cl.id, c.name, l.leave_name, cl.day, cl.created_at, cl.updated_at, l.is_for_permanent_only FROM company_leaves cl 
        inner join companies c on c.id = cl.company_id
        inner join leaves l on l.id = cl.leave_id
        where c.active = true and l.active = true and cl.active = true order by cl.company_id');
        return view('leaveSettings', ['companies' => $companies, 'leaves' => $leaves, 'companyLeaves' => $companyLeaves]);
    }

    public function deleteCompanyLeave(Request $request)
    {
        CompanyLeave::where('id', $request->all()['id'])->update(['active' => false]);
        $companies = Company::where(['active' => true, 'type' => 'company'])->get();
        $leaves = Leave::where(['active' => true])->get();
        $companyLeaves = DB::select('SELECT cl.id, c.name, l.leave_name, cl.day, cl.created_at, cl.updated_at, l.is_for_permanent_only FROM company_leaves cl 
        inner join companies c on c.id = cl.company_id
        inner join leaves l on l.id = cl.leave_id
        where c.active = true and l.active = true and cl.active = true order by cl.company_id');
        return view('leaveSettings', ['companies' => $companies, 'leaves' => $leaves, 'companyLeaves' => $companyLeaves]);
    }

    public function changeProrateStatus(Request $request)
    {
        $id = $request->route()->parameters['id'];
        $no_prorate = Leave::where('id', $id)->first()['no_prorate'];
        Leave::where('id', $id)->update(['no_prorate' => !$no_prorate]);
        
        $companies = Company::where(['active' => true, 'type' => 'company'])->get();
        $leaves = Leave::where(['active' => true])->get();
        $companyLeaves = DB::select('SELECT cl.id, c.name, l.leave_name, cl.day, cl.created_at, cl.updated_at, l.is_for_permanent_only FROM company_leaves cl 
        inner join companies c on c.id = cl.company_id
        inner join leaves l on l.id = cl.leave_id
        where c.active = true and l.active = true and cl.active = true order by cl.company_id');
        return view('leaveSettings', ['companies' => $companies, 'leaves' => $leaves, 'companyLeaves' => $companyLeaves]);
    }
}
