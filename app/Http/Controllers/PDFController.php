<?php

namespace App\Http\Controllers;

use App\Models\Company;
use App\Models\Profile;
use App\Models\User;
use App\Models\Payroll;
use Carbon\Carbon;
use Illuminate\Http\Request;
use DB;
use PDF;
use Illuminate\Support\Facades\Auth;

class PDFController extends Controller
{
    function ContractPDF(Request $request){
        $user_id = $request->route()->parameters['id'];
        $profile = Profile::where(['active' => true, 'user_id' => $user_id])->first();
        $nric = User::where(['active'=>true, 'id'=>$user_id])->first()->user_id;
        $data = (object) array('fullname' => $profile->name, 'effdate' =>date("Y-m-d"), 'salary' => $profile->salary, 'nric' => $nric);
        view()->share('data', $data);
        $pdf = PDF::loadView('contract');
        return $pdf->download($data->nric.'-'.$profile->name.'-contract.pdf');
    }

    function ConfirmationPDF(Request $request)
    {
        $user_id = $request->route()->parameters['id'];
        $profile = Profile::where(['active' => true, 'user_id' => $user_id])->first();
        $nric = User::where(['active' => true, 'id' => $user_id])->first()->user_id;
        $position = Company::where(['active'=>true,'type'=>'position','id'=>$profile->grade_id])->first()->name;
        $data = (object) array('fullname' => $profile->name, 'effdate' => date("Y-m-d"), 'salary' => $profile->salary, 'nric' => $nric , 'address1'=>$profile->address_1, 'address2' => $profile->address_2, 'address3' => $profile->address_3, 'postcode' => $profile->postcode, 'state' => $profile->state, 'country' => $profile->country, 'position'=> $position);
        // dd($data);
        view()->share('data', $data);
        $pdf = PDF::loadView('confirmation');
        return $pdf->download($data->nric. '-confirmation.pdf');
    }

    function requestPayslip(Request $request){
        $showPayslipApproval = Profile::where(['active' => true, 'user_id' => Auth::user()->id])->first()->grade_id == 10;
        Payroll::where(['id' => $request->route()->parameters['id']])->update(['request' => 1]);
        $data = Payroll::where(['active' => true, 'user_id' => Auth::user()->user_id])->orderBy('created_at', 'DESC')->get();
        return view('payroll', ['payroll' => $data, 'showPayslipApproval' => $showPayslipApproval]);
    }

    function cancelRequestPayslip(Request $request){
        $showPayslipApproval = Profile::where(['active' => true, 'user_id' => Auth::user()->id])->first()->grade_id == 10;
        Payroll::where(['id' => $request->route()->parameters['id']])->update(['request' => 0]);
        $data = Payroll::where(['active' => true, 'user_id' => Auth::user()->user_id])->orderBy('created_at', 'DESC')->get();
        return view('payroll', ['payroll' => $data, 'showPayslipApproval' => $showPayslipApproval]);
    }

    function approveRequestPayslip(Request $request){
        $grade = Profile::where(['user_id' => Auth::user()->id])->first();
        Payroll::where(['id' => $request->route()->parameters['id']])->update(['request' => 2]);
        $ranking = Company::where(['id' => $grade['grade_id']])->first();
        $myRanking = $ranking['ranking'];
        if($myRanking == 1){
            $result = Payroll::where(['active' => true, 'request' => 1])->orderBy('created_at', 'DESC')->get();
        }else{
            $result = DB::select('SELECT payrolls.* FROM payrolls
            inner join users on payrolls.user_id = users.user_id
            inner join profiles on users.id = profiles.user_id
            inner join companies on companies.id = profiles.grade_id
            where companies.ranking > '.$myRanking.' and payrolls.request = 1 and payrolls.active = 1;');
        }

        return view('approvalDownloadPayroll', ['payroll' => $result, 'isLeader' => $grade->grade_id == 10]);
    }

    function rejectRequestPayslip(Request $request){
        $grade = Profile::where(['user_id' => Auth::user()->id])->first();
        Payroll::where(['id' => $request->route()->parameters['id']])->update(['request' => 0]);
        $ranking = Company::where(['id' => $grade['grade_id']])->first();
        $myRanking = $ranking['ranking'];
        if($myRanking == 1){
            $result = Payroll::where(['active' => true, 'request' => 1])->orderBy('created_at', 'DESC')->get();
        }else{
            $result = DB::select('SELECT payrolls.* FROM payrolls
            inner join users on payrolls.user_id = users.user_id
            inner join profiles on users.id = profiles.user_id
            inner join companies on companies.id = profiles.grade_id
            where companies.ranking > '.$myRanking.' and payrolls.request = 1 and payrolls.active = 1;');
        }

        return view('approvalDownloadPayroll', ['payroll' => $result, 'isLeader' => $grade->grade_id == 10]);
    }

    function approveRequestPayslipLeader(Request $request){
        $grade = Profile::where(['user_id' => Auth::user()->id])->first();
        if($grade->grade_id != 10) {
            Auth::logout();
            return redirect('/login');
        }
        Payroll::where(['id' => $request->route()->parameters['id']])->update(['request' => 2]);
        $ranking = Company::where(['id' => $grade['grade_id']])->first();
        $myRanking = $ranking['ranking'];
        $result = DB::select('SELECT payrolls.* FROM payrolls
            inner join users on payrolls.user_id = users.user_id
            inner join profiles on users.id = profiles.user_id
            inner join companies on companies.id = profiles.grade_id
            where companies.ranking > '.$myRanking.' and payrolls.request = 1 and payrolls.active = 1;');

        return view('approvalDownloadPayroll', ['payroll' => $result, 'isLeader' => $grade->grade_id == 10]);
    }

    function rejectRequestPayslipLeader(Request $request){
        $grade = Profile::where(['user_id' => Auth::user()->id])->first();
        if($grade->grade_id != 10) {
            Auth::logout();
            return redirect('/login');
        }
        Payroll::where(['id' => $request->route()->parameters['id']])->update(['request' => 0]);
        $ranking = Company::where(['id' => $grade['grade_id']])->first();
        $myRanking = $ranking['ranking'];
        $result = DB::select('SELECT payrolls.* FROM payrolls
            inner join users on payrolls.user_id = users.user_id
            inner join profiles on users.id = profiles.user_id
            inner join companies on companies.id = profiles.grade_id
            where companies.ranking > '.$myRanking.' and payrolls.request = 1 and payrolls.active = 1;');

        return view('approvalDownloadPayroll', ['payroll' => $result, 'isLeader' => $grade->grade_id == 10]);
    }

    function approvalDownloadPayroll(){
        $grade = Profile::where(['user_id' => Auth::user()->id])->first();
        $ranking = Company::where(['id' => $grade['grade_id']])->first();
        $myRanking = $ranking['ranking'];
        if($myRanking == 1){
            $result = Payroll::where(['active' => true, 'request' => 1])->orderBy('created_at', 'DESC')->get();
        }else{
            $result = DB::select('SELECT payrolls.* FROM payrolls
            inner join users on payrolls.user_id = users.user_id
            inner join profiles on users.id = profiles.user_id
            inner join companies on companies.id = profiles.grade_id
            where companies.ranking > '.$myRanking.' and payrolls.request = 1 and payrolls.active = 1;');
        }

        return view('approvalDownloadPayroll', ['payroll' => $result, 'isLeader' => $grade->grade_id == 10]);
    }

    function approvalDownloadPayrollLeader(){
        $grade = Profile::where(['user_id' => Auth::user()->id])->first();
        if($grade->grade_id != 10) {
            Auth::logout();
            return redirect('/login');
        }
        $ranking = Company::where(['id' => $grade['grade_id']])->first();
        $myRanking = $ranking['ranking'];
        if($myRanking == 1){
            $result = Payroll::where(['active' => true, 'request' => 1])->orderBy('created_at', 'DESC')->get();
        }else{
            $result = DB::select('SELECT payrolls.* FROM payrolls
            inner join users on payrolls.user_id = users.user_id
            inner join profiles on users.id = profiles.user_id
            inner join companies on companies.id = profiles.grade_id
            where companies.ranking > '.$myRanking.' and payrolls.request = 1 and payrolls.active = 1;');
        }

        return view('approvalDownloadPayroll', ['payroll' => $result, 'isLeader' => $grade->grade_id == 10]);
    }

    function generatePayslip(Request $request) {
        $payroll_id = $request->route()->parameters['id'];
        $payrollInfo = Payroll::where(['id' => $payroll_id, 'request' => 2])->first();
        $data = (object)array(
        'empnirc' => $payrollInfo->empnirc,
        'empname' => $payrollInfo->empname,
        'serialno' => $payrollInfo->empno,
        'basic' => $payrollInfo->basic,
        'npl' => $payrollInfo->npl,
        'addpay' => $payrollInfo->addpay,
        'ot' => $payrollInfo->ot,
        'bonus' => $payrollInfo->bonus,
        'meal' => $payrollInfo->meal,
        'claims' => $payrollInfo->claims,
        'inc1' => $payrollInfo->inc1,
        'other2' => $payrollInfo->other2,
        'other3' => $payrollInfo->other3,
        'other4' => $payrollInfo->other4,
        'other5' => $payrollInfo->other5,
        'grosspay' => $payrollInfo->grosspay,
        'empepf' => $payrollInfo->empepf,
        'empsocso' => $payrollInfo->empsocso,
        'empeis' => $payrollInfo->empeis,
        'emppcb' => $payrollInfo->emppcb,
        'nettpay' => $payrollInfo->nettpay,
        'repf' => $payrollInfo->repf,
        'rsocso' => $payrollInfo->rsocso,
        'reis' => $payrollInfo->reis,
        'month' => $payrollInfo->month,
        'year' => $payrollInfo->year,
        'created_at' => $payrollInfo->created_at,
        'hostel' => $payrollInfo->hostel,
        'dispute' => $payrollInfo->dispute,
        'referral' => $payrollInfo->referral,
        'company' => $payrollInfo->company,
        'plencash' => $payrollInfo->plencash,
        'projallow' => $payrollInfo->projallow,
        'cnyspcom' => $payrollInfo->cnyspcom,
        'print_on' => date("Y-m-d")
        );
        view()->share('data', $data);
        Payroll::where(['id' => $request->route()->parameters['id']])->update(['request' => 0]);
        if($payrollInfo->other5 == "2.00"){
            $pdf = PDF::loadView('paymentVoucherTemplate');
            return $pdf->download($data->empnirc. '-paymentVoucher.pdf');
        }else{
            $pdf = PDF::loadView('payslipTemplate');
            return $pdf->download($data->empnirc. '-payslip.pdf');
        }
        
    }
}
