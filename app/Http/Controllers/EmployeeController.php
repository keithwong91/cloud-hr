<?php

namespace App\Http\Controllers;

use App\Models\Company;
use App\Models\Profile;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Symfony\Component\Console\Input\Input;
use Illuminate\Support\Facades\Hash;

class EmployeeController extends Controller
{
    public function viewprofile(Request $request){
        $user_id = $request->route()->parameters['id'];
        $profile = DB::select('SELECT p.* , u.user_id as nric FROM profiles p inner join users u on u.id = p.user_id where u.active = true and u.id = ' . $user_id);
        $companies = Company::where(['active' => true, 'type' => 'company'])->get();
        $departments = Company::where(['active' => true, 'type' => 'department'])->get();
        $positions = Company::where(['active' => true, 'type' => 'position'])->get();
        return view('viewProfile', ['profile' => $profile[0], 'companies' => $companies, 'departments' => $departments, 'positions' => $positions]);
    }

    public function updateUserProfile(Request $request) {

        $request->offsetUnset('_token');
        Profile::updateOrCreate(
            ['user_id' => $request['user_id']],
            $request->all()
        );
        $message = 'Profile updated';
        $user_id = $request['user_id'];
        $profile = DB::select('SELECT p.* , u.user_id as nric FROM profiles p inner join users u on u.id = p.user_id where u.active = true and u.id = ?',[$user_id]);
        $companies = Company::where(['active' => true, 'type' => 'company'])->get();
        $departments = Company::where(['active' => true, 'type' => 'department'])->get();
        $positions = Company::where(['active' => true, 'type' => 'position'])->get();
        return view('viewProfile', ['profile' => $profile[0], 'companies' => $companies, 'departments' => $departments, 'positions' => $positions, 'message' => $message]);
    }

    public function disableProfile(Request $request)
    {
        $user_id = $request->route()->parameters['id'];
        User::where('id', $user_id)->update(['active' => false]);
        $profiles = DB::select('SELECT p.* , u.user_id as nric, u.is_admin FROM profiles p inner join users u on u.id = p.user_id where u.active = true');
        return view('employee', ['profiles' => $profiles]);
    }

    public function setAsAdmin(Request $request)
    {
        $user_id = $request->route()->parameters['id'];
        $is_admin = User::where('id', $user_id)->first()['is_admin'];
        User::where('id', $user_id)->update(['is_admin' => !$is_admin]);
        
        $profiles = DB::select('SELECT p.* , u.user_id as nric, u.is_admin FROM profiles p inner join users u on u.id = p.user_id where u.active = true');
        return view('employee', ['profiles' => $profiles]);
    }

    public function resetPassword(Request $request)
    {
        $user_id = $request->route()->parameters['id'];
        User::where('id', $user_id)->update(['password' => Hash::make('999999999')]);
        $profiles = DB::select('SELECT p.* , u.user_id as nric, u.is_admin FROM profiles p inner join users u on u.id = p.user_id where u.active = true');
        return view('employee', ['profiles' => $profiles]);
    }
}
