<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Payroll extends Model
{
    use HasFactory;

    protected $fillable = [
            'user_id',
            'empno',
            'empnirc',
            'empname',
            'basic',
            'npl',
            'addpay',
            'ot',
            'bonus',
            'meal',
            'claims',
            'inc1',
            'other2',
            'other3',
            'other4',
            'other5',
            'grosspay',
            'empepf',
            'empsocso',
            'empeis',
            'emppcb',
            'nettpay',
            'repf',
            'rsocso',
            'reis',
            'month',
            'year',
            'hostel',
            'dispute',
            'company',
            'request',
            'projallow',
            'cnyspcom',
            'referral'
    ];
}
