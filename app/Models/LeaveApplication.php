<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LeaveApplication extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'leave_id',
        'from',
        'to',
        'day',
        'daytime',
        'reason',
        'attachment_name',
        'attachment',
        'remark',
        'status',
    ];
}
