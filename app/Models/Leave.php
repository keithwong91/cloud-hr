<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Leave extends Model
{
    use HasFactory;

    protected $fillable = [
        'leave_name',
        'day',
        'is_for_permanent_only',
        'apply_for_all_companies',
        'no_prorate',
        'active',
    ];
}
