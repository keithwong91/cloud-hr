<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'name',
        'email',
        'phone',
        'dob',
        'address_1',
        'address_2',
        'address_3',
        'postcode',
        'state',
        'country',
        'company_id',
        'grade_id',
        'department_id',
        'confirm_at',
        'is_permanent',
        'active',
        'salary'
    ];
}
